import { registerBlockVariation } from "@wordpress/blocks";

/**
 * documentation :  https://make.wordpress.org/core/2020/02/27/introduce-block-variations-api/
 */
registerBlockVariation("core/cover", {
    name: "page-header",
    title: "Page Header",
    description: "Page header with h1, breadcrumb and button",
    category: "vc-category", // this does not seem to work yet
    icon: "image", // @TODO use better icon
  //scope: ['inserter'],
    attributes: {
        dimRatio: 0,
        align: "full",
        className: "page-header",
        minHeight: 300
    },
    innerBlocks: [
    ["core/heading", { placeholder: "Enter page title here...", level: 1 }],
    [
      "core/buttons",
      {},
      [["core/button", { title: "Contact", text: "Contact", url: "/contact" }]]
    ]
    ]
});
