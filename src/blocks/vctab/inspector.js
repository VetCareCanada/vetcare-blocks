/**
 * Wordpress dependencies
 */
import {Component} from "@wordpress/element";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody, TextControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";

class Inspector extends Component {
    render() {
        const {attributes} = this.props;
        const {contentId} = attributes;

        return (
            <InspectorControls>
                <PanelBody title={__("Fancy Heading settings", "vc-blocks")}>
                    <TextControl
                        label={__("Content Id", "vc-blocks")}
                        value={contentId}
                        onChange={value => this.props.setAttributes({contentId: value})}
                    />
                </PanelBody>
            </InspectorControls>
        );
    }
}

export default Inspector;
