import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import {
    BlockControls,
    MediaUploadCheck,
    InnerBlocks,
    InspectorControls,
    MediaUpload,
    MediaPlaceholder
} from "@wordpress/block-editor";
import {
    PanelBody,
    RangeControl,
    ToggleControl,
    Button,
    Toolbar
} from "@wordpress/components";
import classnames from "classnames";

const attributes = {};

registerBlockType("vc-blocks/vctabs", {
    title: __("VC Tabs", "vc-blocks"),
    description: __("VC Tabs", "vc-blocks"),
    icon: "grid-view",
    category: "vc-category",
    supports: {
        html: false,
        align: ["full"]
    },
    keywords: [__("VC Tabs", "vc-blocks")],

    attributes,

    edit({className, attributes, setAttributes, clientId}) {
        //console.log(backgroundImage);
        const classes = classnames({
            "vctabs": true,
            [className]: !!className
        });
        return (
            <div className={classes}>
                <div className="vctabs-wrapper">
                    <InnerBlocks
                        allowedBlocks={["vc-blocks/vctab"]}
                        template={[["vc-blocks/vctab"], ["vc-blocks/vctab"]]}
                        parentClientId={clientId}
                    />
                </div>
            </div>
        );
    },
    save(props) {
        const {attributes} = props;
        //console.log(props);
        const {columns, addBackground, backgroundImage} = attributes;

        const classes = classnames({
            "vctabs": true,
        });
        return (
            <div className={classes}>
                <div className="vctabs-wrapper">
                    <InnerBlocks.Content/>
                </div>
            </div>
        );
    }
});
