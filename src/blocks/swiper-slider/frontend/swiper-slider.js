const swiperSliders = document.querySelectorAll(".wp-block-vc-blocks-swiper-slider");
swiperSliders.forEach(swiperSlider => {
    let swiperConfig = swiperSlider.dataset.swiperConfig;
    swiperConfig = JSON.parse(swiperConfig);
    new Swiper(swiperSlider, swiperConfig);
});


