/**
 * Wordpress Dependencies
 */
import {Component} from "@wordpress/element";
import {
    BlockControls,
    InspectorControls,
    MediaUpload,
    MediaUploadCheck
} from "@wordpress/block-editor";
import {Button, Toolbar, TextControl, PanelBody} from "@wordpress/components";
import {__} from "@wordpress/i18n";

class ToolbarControls extends Component {
    onSelectBackgroundImage = image => {
        const {id, url} = image;
        this.props.setAttributes({
            mediaId: id,
            mediaUrl: url
        });
    };

    removeBackgroundImage = () => {
        const {setAttributes} = this.props;
        setAttributes({
            mediaUrl: "",
            mediaId: null
        });
    };

    onChangeVideoUrl = videoUrl => {

        const {setAttributes} = this.props;
        setAttributes({
            videoUrl: videoUrl
        });
    }

    render() {
        const {attributes} = this.props;
        const {mediaId, videoUrl} = attributes;
        return (
            <>
                <BlockControls>
                    <Toolbar>
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={this.onSelectBackgroundImage}
                                allowedTypes={["image"]}
                                value={mediaId}
                                render={({open}) => {
                                    return (
                                        <Button
                                            className="components-icon-button components-toolbar__control"
                                            label={__("Add/Edit background Image", "vc-blocks")}
                                            onClick={open}
                                            icon="format-image"
                                        />
                                    );
                                }}
                            />
                        </MediaUploadCheck>
                    </Toolbar>
                </BlockControls>

                <InspectorControls>
                    <PanelBody title={__("Slide options", "vc-blocks")}>
                        <TextControl
                            label="Youtube Video URL"
                            value={videoUrl}
                            help="youtube video url for selected slide"
                            onChange={(e) => this.onChangeVideoUrl(e)}
                        />
                    </PanelBody>
                </InspectorControls>
            </>
        );
    }
}

export default ToolbarControls;
