/**
 * Wordpress Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";

/**
 * Internal Dependencies
 */
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/swiper-single-slide", {
    title: __("Swiper Single Slide", "vc-blocks"),
    description: __("Swiper Single Slide", "vc-blocks"),
    category: "vc-category",
    keywords: [__("Swiper Single Slide", "vc-blocks"), __("Slider", "vc-blocks")],
    icon: "slides",
    parent: ["vc-blocks/swiper-slider"],
    attributes: {
        galleryMode: {
            type: "boolean",
            default: false
        },
        mediaId: {
            type: "number"
        },
        mediaAlt: {
            type: "string",
            source: "attribute",
            selector: "img",
            attribute: "alt"
        },
        mediaUrl: {
            type: "string",
            source: "attribute",
            selector: "img",
            attribute: "src"
        },
        videoUrl: {
            type: "string",
            source: "attribute",
            selector: "img",
            attribute: "data-videourl"
        }
    },
    edit,
    save
});
