/**
 * Wordpress Dependencies
 */
import {InnerBlocks} from "@wordpress/block-editor";
import classnames from "classnames";

function save(props) {
    const {className, attributes} = props;
    const {galleryMode, mediaId, videoUrl, mediaUrl, mediaAlt} = attributes;
    const classes = classnames({
        [className]: !!className,
        'swiper-slide': true,
        'video-link': videoUrl
    });
    return (
        <div className={classes}>
            <div className="swiper-slide__img">
                <img
                    src={mediaUrl}
                    alt={mediaAlt}
                    data-videoUrl={videoUrl}
                    className={mediaId ? `wp-image-${mediaId}` : null}
                />
                {videoUrl && <i className="fas fa-play-circle"></i>}
            </div>
            {!galleryMode && (
                <div className="swiper-slide__content">
                    <InnerBlocks.Content/>
                </div>
            )}
        </div>
    );
}

export default save;
