/**
 * Wordpress Dependencies
 */
import {Component} from "@wordpress/element";
import {BlockControls} from "@wordpress/block-editor";
import {Toolbar, ToolbarButton} from "@wordpress/components";
import {__} from "@wordpress/i18n";

class ToolbarControls extends Component {
  render() {
    const {onPreviewClick, preview} = this.props;
    return (
      <BlockControls>
        <Toolbar>
          <ToolbarButton
            className="components-icon-button components-toolbar__control"
            label={preview === true ? __("Edit", "vc-blocks") : __("Preview", "vc-blocks")}
            onClick={onPreviewClick}
            icon={preview === true ? "edit" : "visibility"}
          />
        </Toolbar>
      </BlockControls>
    );
  }
}

export default ToolbarControls;
