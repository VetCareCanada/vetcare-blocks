/**
 * Wordpress Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";

/**
 * Internal Dependencies
 */
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/swiper-slider", {
    title: __("Swiper Slider", "vc-blocks"),
    description: __("Swiper Slider", "vc-blocks"),
    category: "vc-category",
    keywords: [__("Swiper Slider", "vc-blocks"), __("Slider", "vc-blocks")],
    icon: "slides",
    attributes: {
        dimRatio: {
            type: "number",
            "default": 50
        },
        galleryMode: {
            type: "boolean",
            default: false
        },
        overlayColor: {
            type: "string"
        },
        dotsColor: {
            type: "string"
        },
        arrowsColor: {
            type: "string"
        },
        sliderConfig: {
            type: "object",
            default: {
                navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
                pagination: {clickable: true, el: ".swiper-pagination"},
                speed: 300,
                autoplay: {
                    delay: 4000
                }
            }
        }
    },
    supports: {
        align: ["full"]
    },
    edit,
    save
});
