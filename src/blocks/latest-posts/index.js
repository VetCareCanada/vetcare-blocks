import "./styles.editor.scss";

import Inspector from "./inspectorControls";
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import ServerSideRender from '@wordpress/server-side-render';
import {useBlockProps} from '@wordpress/block-editor';

registerBlockType("vc-blocks/latest-posts", {
    title: __("Latest Posts", "vc"),
    description: __("Block that shows a list of the latest posts", "vc-blocks"),
    icon: "admin-post",
    category: "vc-category",
    //edit: edit,
    edit: function (props) {
        const blockProps = useBlockProps();

        const onChangeNumberOfPosts = numberOfPosts => {
            props.setAttributes({numberOfPosts});
        };
        const onChangePostType = postType => {
            props.setAttributes({postType});
        };

        return (
            <>
                <Inspector
                    {...props}
                    onChangeNumberOfPosts={onChangeNumberOfPosts}
                    onChangePostType={onChangePostType}/>
                <div {...blockProps}>
                    <ServerSideRender
                        block="vc-blocks/latest-posts"
                        attributes={props.attributes}
                    />
                </div>

            </>

        );

    },
    attributes: {
        numberOfPosts: {
            type: 'number',
            default: 2
                },
                postType: {
                    type: 'string'
                },
                },
                save() {
                    return null;
                }
                });
