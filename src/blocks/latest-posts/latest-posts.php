<?php

function vc_blocks_render_latest_posts_block($attributes): string
{
    $postType = $attributes['postType'];
    $currentPostId = get_the_ID();

    $args = array(
        'post_type' => $postType,
        'posts_per_page' => $attributes['numberOfPosts']
    );

    $classes = 'wp-block-vc-blocks-latest-posts ';
    $classes .= 'has-' . $attributes['numberOfPosts'] . '-columns ';
    if (isset($attributes['className'])) {
        $classes .= $attributes['className'];
    }
    $query = new WP_Query($args);
    $posts = '';
    if ($query->have_posts()) {
        $posts .= '<div class=" ' . $classes . ' "> ';
        while ($query->have_posts()) {
            $query->the_post();

            if (get_the_ID() == $currentPostId) {
                continue;
            }

            $serviceIconArray = get_field('service_icon', get_the_ID());
            $serviceIcon = ($serviceIconArray && $serviceIconArray['url']) ?
                '<span class="vc-post__icon"><img src="'.$serviceIconArray['url'].'"/></span>' : '';

            $posts .=
                '<div class="vc-post vc-post--' . $postType . '">                    
                    <div class="vc-post__body">
                        <div class="vc-post__title"> ' .
                $serviceIcon. ' ' .
                get_the_title() .
                '</div> 
                    </div>
                </div>';
        }

        $posts .= '</div>';
        wp_reset_postdata();
        return $posts;
    } else {
        return '<div>' . __("No Posts Found", "vc-blocks") . '</div>';
    }
}
