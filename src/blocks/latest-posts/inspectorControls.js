import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody, RangeControl, SelectControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";

const Inspector = (props) => {
    console.log(props);
    const {attributes} = props;
    return (
        <InspectorControls>
            <PanelBody title={__("Posts Settings", "vc-blocks")}>
                <RangeControl
                    value={attributes.numberOfPosts}
                    onChange={props.onChangeNumberOfPosts}
                    min={0}
                    max={10}
                    label={__("Number of Posts", "vc-blocks")}
                />
                <SelectControl
                    label="Size"
                    value={attributes.postType}
                    options={
                        [
                            {label: 'Post', value: 'post'},
                            {label: 'Service', value: 'service'},
                        ]
                    }
                    onChange={props.onChangePostType}
                />
            </PanelBody>
        </InspectorControls>
    );
}
export default Inspector;