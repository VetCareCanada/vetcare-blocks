import { Component } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import { InspectorControls, InnerBlocks } from "@wordpress/block-editor";
import { PanelBody, SelectControl } from "@wordpress/components";

class Edit extends Component {
  onChangeAlignType = alignType => {
    this.props.setAttributes({ alignType });
  };
  render() {
    const { attributes } = this.props;
    const { alignType } = attributes;
    const alignTypeOptions = [
      {
        label: "Left",
        value: "align-ctas-left"
      },
      {
        label: "Center",
        value: "align-ctas-center"
      },
      {
        label: "Right",
        value: "align-ctas-right"
      }
    ];
    return (
      <>
        <InspectorControls>
          <PanelBody title={__("Align Type", "vc-blocks")}>
            <SelectControl
              label={__("Align Type", "vc-blocks")}
              value={alignType}
              options={alignTypeOptions}
              onChange={this.onChangeAlignType}
            />
          </PanelBody>
        </InspectorControls>
        <div className={`multi-cta ${alignType}`}>
          <div>
            <InnerBlocks allowedBlocks={["core/button"]} />
          </div>
        </div>
      </>
    );
  }
}

export default Edit;
