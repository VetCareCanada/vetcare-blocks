import { Component } from "@wordpress/element";
import { InnerBlocks } from "@wordpress/block-editor";

class Save extends Component {
  render() {
    const { attributes } = this.props;
    const { alignType } = attributes;
    return (
      <div className={`multi-cta ${alignType}`}>
        <div>
          <InnerBlocks.Content />
        </div>
      </div>
    );
  }
}

export default Save;
