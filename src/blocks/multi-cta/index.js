import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
  alignType: {
    type: "string",
    source: "attributes",
    attribute: "class",
    selector: ".multi-cta"
  }
};

registerBlockType("vc-blocks/multi-cta", {
  title: __("Multiple CTA", "vc-blocks"),
  description: __("Multiple CTA buttons", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("CTA", "vc-blocks"),
    __("Button", "vc-blocks"),
    __("Call to Action", "vc-blocks")
  ],
  icon: "admin-links",
  attributes,
  edit,
  save
});
