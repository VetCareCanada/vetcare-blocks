import {Component, RawHTML} from "@wordpress/element";
import classnames from "classnames";
import {IconButton, Toolbar} from "@wordpress/components";
import {
  BlockControls,
  ContrastChecker,
  InnerBlocks,
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
  PanelColorSettings,
  withColors,
  BlockVerticalAlignmentToolbar,
  PlainText,
  RichText
} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";

class Edit extends Component {
  onSelectImage = image => {
    const {id, alt, url} = image;
    this.props.setAttributes({mediaId: id, mediaAlt: alt, mediaUrl: url});
  };
  onSelectURL = mediaUrl => {
    this.props.setAttributes({
      mediaUrl,
      mediaId: null,
      mediaAlt: ""
    });
  };
  removeImage = () => {
    const {setAttributes} = this.props;
    setAttributes({
      mediaUrl: "",
      mediaAlt: "",
      mediaId: null
    });
  };

  onChangeAlt = mediaAlt => {
    this.props.setAttributes({mediaAlt});
  };

  onSelectBackgroundImage = image => {
    const {id, url} = image;
    this.props.setAttributes({
      backgroundImageId: id,
      backgroundImageUrl: url
    });
  };

  removeBackgroundImage = () => {
    const {setAttributes} = this.props;
    setAttributes({
      backgroundImageUrl: "",
      backgroundImageId: null
    });
  };

  render() {
    console.log("GURPREET");
    const {
      attributes,
      className,
      setAttributes,
      backgroundColor,
      textColor,
      setBackgroundColor,
      setTextColor
    } = this.props;
    const {
      title,
      subTitle,
      iframe,
      sameHeight,
      mapHorizontalAlignment,
      mapVerticalAlignment,
      backgroundImageId,
      backgroundImageUrl
    } = attributes;
    const ALLOWED_BLOCKS = [
      "core/paragraph",
      "core/heading",
      "core/button",
      "core/list",
      "core/shortcode",
      "gravityforms/form"
    ];
    const TEMPLATE = [
      [
        "core/paragraph",
        {
          content:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
        }
      ]
    ];
    const sameHeightToolbarControls = [
      [
        {
          icon: "columns",
          title: __("Same Height", "vc-blocks"),
          isActive: sameHeight,
          onClick: () => setAttributes({sameHeight: !sameHeight})
        }
      ]
    ];
    const alignmentToolbarControls = [
      [
        {
          icon: "align-pull-left",
          title: __("Show image on left", "vc-blocks"),
          isActive: mapHorizontalAlignment === "left",
          onClick: () => setAttributes({mapHorizontalAlignment: "left"})
        },
        {
          icon: "align-pull-right",
          title: __("Show image on right", "vc-blocks"),
          isActive: mapHorizontalAlignment === "right",
          onClick: () => setAttributes({mapHorizontalAlignment: "right"})
        }
      ]
    ];
    const classes = classnames({
      "map-text": true,
      "map-text--sameHeight": sameHeight
    });

    const innerClasses = classnames({
      "map-text__inner": true,
      [className]: !!className,
      "align-items-start": !sameHeight && mapVerticalAlignment === "top",
      "align-items-center": !sameHeight && mapVerticalAlignment === "center",
      "align-items-end": !sameHeight && mapVerticalAlignment === "bottom"
    });
    const mapClasses = classnames({
      "map-text__map": true
    });
    const contentClasses = classnames({
      "map-text__content": true,
      "order-md-12": mapHorizontalAlignment === "right",
      "justify-content-start": sameHeight && mapVerticalAlignment === "top",
      "justify-content-center": sameHeight && mapVerticalAlignment === "center",
      "justify-content-end": sameHeight && mapVerticalAlignment === "bottom"
    });
    const contentStyles = {};
    const styles = {
      color: textColor.color
    };
    if (sameHeight) {
      contentStyles.backgroundColor = backgroundColor.color;
    } else {
      styles.backgroundColor = backgroundColor.color;
    }
    if (backgroundImageUrl) {
      styles.backgroundImage = `url(${backgroundImageUrl})`;
    }

    //console.log(this.props);
    return (
      <>
        <InspectorControls>
          <PanelColorSettings
            title={__("Color Settings", "vc-blocks")}
            initialOpen={false}
            colorSettings={[
              {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __("Background Color", "vc-blocks")
              },
              {
                value: textColor.color,
                onChange: setTextColor,
                label: __("Text Color", "vc-blocks")
              }
            ]}
          >
            <ContrastChecker
              textColor={textColor.color}
              backgroundColor={backgroundColor.color}
            />
          </PanelColorSettings>
        </InspectorControls>
        <BlockControls>
          <Toolbar>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={this.onSelectBackgroundImage}
                allowedTypes={["image"]}
                value={backgroundImageId}
                render={({open}) => {
                  return (
                    <IconButton
                      className="components-icon-button components-toolbar__control"
                      label={__("Add/Edit background Image", "vc-blocks")}
                      onClick={open}
                      icon="format-image"
                    />
                  );
                }}
              />
            </MediaUploadCheck>

            {backgroundImageUrl && (
              <IconButton
                className="components-icon-button components-toolbar__control"
                label={__("Remove Background Image", "vc-blocks")}
                onClick={this.removeBackgroundImage}
                icon="trash"
              />
            )}
          </Toolbar>
          <Toolbar controls={sameHeightToolbarControls}/>
          <BlockVerticalAlignmentToolbar
            onChange={alignment =>
              setAttributes({mapVerticalAlignment: alignment})
            }
            value={mapVerticalAlignment}
          />
          <Toolbar controls={alignmentToolbarControls}/>
        </BlockControls>
        <div className={classes} style={styles}>
          <div className="map-text__titles">
            <RichText
              placeholder="Title here..."
              tagName="h2"
              value={title}
              onChange={value => setAttributes({title: value})}
            />
            <RichText
              placeholder="Sub Title here..."
              tagName="h4"
              value={subTitle}
              onChange={value => setAttributes({subTitle: value})}
            />
          </div>
          <div className={innerClasses}>
            <div className={contentClasses} style={contentStyles}>
              <InnerBlocks
                allowedBlocks={ALLOWED_BLOCKS}
                template={TEMPLATE}
              />
            </div>
            <div className={mapClasses}>
              {attributes.iframe && <RawHTML>{attributes.iframe}</RawHTML>}
              {
                <PlainText
                  placeholder="Enter the iFrame url here..."
                  rows={2}
                  value={iframe}
                  onChange={content => setAttributes({iframe: content})}
                />
              }
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withColors("backgroundColor", {textColor: "color"})(Edit);
