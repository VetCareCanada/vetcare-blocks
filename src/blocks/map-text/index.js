import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/map-text", {
  title: __("Map & Text", "vc-blocks"),
  description: __("Map & Text", "vc-blocks"),
  category: "vc-category",
  keywords: [__("Map & Text", "vc-blocks"), __("Map", "vc-blocks")],
  icon: (
    <svg
      width="24"
      height="24"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      role="img"
      aria-hidden="true"
      focusable="false"
    >
      <path d="M13 17h8v-2h-8v2zM3 19h8V5H3v14zM13 9h8V7h-8v2zm0 4h8v-2h-8v2z"></path>
    </svg>
  ),
  attributes: {
    title: {
      type: "string",
      source: "html",
      selector: ".map-text__titles h2"
    },
    subTitle: {
      type: "string",
      source: "html",
      selector: ".map-text__titles h4"
    },
    iframe: {
      type: "string"
      //source: 'html',
      //selector: '.map-text__map'
    },
    sameHeight: {
      type: "boolean",
      default: false
    },
    mapHorizontalAlignment: {
      type: "string",
      default: "right"
    },
    mapVerticalAlignment: {
      type: "string",
      default: "top"
    },
    backgroundColor: {
      type: "string"
    },
    textColor: {
      type: "string"
    },
    customBackgroundColor: {
      type: "string"
    },
    customTextColor: {
      type: "string"
    },
    backgroundImageId: {
      type: "number"
    },
    backgroundImageUrl: {
      type: "string"
    }
  },
  edit,
  save
});
