import "./style.editor.scss";
import "./parent";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
  clientId: {
    type: "string"
  },
  title: {
    type: "string",
    default: "Tab Title"
  },
  content: {
    type: "string"
  }
};

registerBlockType("vc-blocks/tab", {
  title: __("Tab", "vc-blocks"),
  description: __("Tab", "vc-blocks"),
  category: "vc-category",
  icon: "wordpress",
  /*supports: {
    reusable: false
  },*/
  attributes,
  edit,
  save
});
