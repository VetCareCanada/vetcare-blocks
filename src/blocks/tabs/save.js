import classnames from "classnames";
import { InnerBlocks } from "@wordpress/block-editor";
import slugify from "slugify";

function save(props) {
  const { attributes, className } = props;
  const { title } = attributes;
  const tabSlug = slugify(title == null ? "" : title);
  const classes = classnames({
    "vc-tab": true,
    [className]: !!className,
    "tab-pane": true,
    fade: true,
    "active show": tabSlug === "Overview"
  });
  return (
    <div
      className={classes}
      id={`vcTabPane-${tabSlug}`}
      role="tabpanel"
      aria-labelledby={`vcTab-${tabSlug}`}
    >
      <InnerBlocks.Content />
    </div>
  );
}

export default save;
