import { Component } from "@wordpress/element";
import { InnerBlocks } from "@wordpress/block-editor";
import classnames from "classnames";
import slugify from "slugify";

class ParentSave extends Component {
  render() {
    //console.log(this.props);
    const { attributes } = this.props;
    const { clientId, tabs } = attributes;
    let tabCounter = 0;
    return (
      <div>
        <ul className="nav nav-tabs" id={`vcTabs-${clientId}`} role="tablist">
          {tabs.map(tab => {
            tabCounter++;
            let tabSlug = slugify(tab.title);
            const navLinkClasses = classnames({
              "nav-link": true,
              active: tabCounter === 1
            });
            return (
              <li className="nav-item" key={`vcTab-${tabSlug}`}>
                <a
                  id={`vcTab-${tabSlug}`}
                  className={navLinkClasses}
                  data-toggle="tab"
                  href={`#vcTabPane-${tabSlug}`}
                  role="tab"
                  aria-controls={`vcTabPane-${tabSlug}`}
                  aria-selected={tabCounter === 1 ? "true" : "false"}
                >
                  <span>{tab.title}</span>
                </a>
              </li>
            );
          })}
        </ul>
        <div className="tab-content">
          <InnerBlocks.Content />
        </div>
      </div>
    );
  }
}

export default ParentSave;
