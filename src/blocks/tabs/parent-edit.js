import { Component } from "@wordpress/element";
import { withDispatch, withSelect } from "@wordpress/data";
import { RichText, InnerBlocks } from "@wordpress/block-editor";
import { compose } from "@wordpress/compose";
import slugify from "slugify";
import classnames from "classnames";

class ParentEdit extends Component {
  componentDidMount() {
    const { vcInnerBlocks, setAttributes } = this.props;
    const newTabs = vcInnerBlocks.map(block => {
      return {
        clientId: block.clientId,
        title: block.attributes.title
      };
    });
    setAttributes({ tabs: newTabs });
    //console.log("COMPONENT MOUNTED", this.props);
  }

  tabClicked = event => {
    const tabPaneId = event.currentTarget.getAttribute("href").replace("#", "");

    const allTabPanes = document.querySelectorAll(".vc-tab");

    allTabPanes.forEach(item => {
      //console.log(tabPaneId, item.getAttribute("id"));
      if (item.getAttribute("id") === tabPaneId) {
        item.closest('[data-type="vc-blocks/tab"]').classList.remove("d-none");
        return;
      }
      item.classList.remove("show", "active");
      item.closest('[data-type="vc-blocks/tab"]').classList.add("d-none");
    });
  };

  tabDeleteClicked = clientId => {
    //console.log(clientId);
    const { attributes, setAttributes } = this.props;
    const { tabs } = attributes;
    const newTabs = tabs.filter(tab => tab.clientId !== clientId);
    setAttributes({ tabs: newTabs });
    this.props.onTabDelete(clientId);
  };

  render() {
    const { className, setAttributes, clientId, vcInnerBlocks } = this.props;
    let tabCounter = 0;
    //console.log(this.props);
    // Set the block identifier
    setAttributes({ clientId: clientId });
    return (
      <div className={`${className}`}>
        <ul className="nav nav-tabs" id={`vcTabs-${clientId}`} role="tablist">
          {vcInnerBlocks.map(block => {
            tabCounter++;
            let tabSlug = slugify(block.attributes.title);
            const navLinkClasses = classnames({
              "nav-link": true,
              active: tabCounter === 1
            });
            return (
              <li className="nav-item" key={`vcTab-${tabSlug}`}>
                <a
                  id={`vcTab-${tabSlug}`}
                  className={navLinkClasses}
                  data-toggle="tab"
                  href={`#vcTabPane-${tabSlug}`}
                  role="tab"
                  aria-controls="home"
                  aria-selected={tabCounter === 1 ? "true" : "false"}
                  onClick={this.tabClicked}
                >
                  <RichText
                    tagName="span"
                    value={block.attributes.title}
                    placeholder="Tab title here..."
                    onChange={content =>
                      this.props.onTabTitleChange(content, block.clientId)
                    }
                  />
                  <div
                    onClick={() => this.tabDeleteClicked(block.clientId)}
                    className="remove-tab"
                  >
                    x
                  </div>
                </a>
              </li>
            );
          })}
        </ul>

        <div className="tab-content">
          <InnerBlocks
            allowedBlocks={["vc-blocks/tab"]}
            template={[
              ["vc-blocks/tab", { title: "tab 1" }],
              ["vc-blocks/tab", { title: "tab 2" }]
            ]}
            renderAppender={() => <InnerBlocks.ButtonBlockAppender />}
          />
        </div>
      </div>
    );
  }
}

export default compose([
  withSelect((select, props) => {
    const { clientId } = props;
    const coreEditor = select("core/block-editor");
    const vcInnerBlocks = coreEditor.getBlock(clientId).innerBlocks;
    return {
      vcInnerBlocks: vcInnerBlocks
    };
  }),
  withDispatch((dispatch, props) => {
    return {
      onTabsLoad: () => {
        const { vcInnerBlocks, setAttributes } = props;
        const newTabs = vcInnerBlocks.map(block => {
          return {
            clientId: block.clientId,
            title: block.attributes.title
          };
        });
        //console.log(newTabs);
        setAttributes({ tabs: newTabs });
      },
      onTabTitleChange: (content, tabClientId) => {
        const { vcInnerBlocks, setAttributes } = props;
        //console.log(props);
        dispatch("core/block-editor").updateBlockAttributes(tabClientId, {
          title: content
        });
        const newTabs = vcInnerBlocks.map(block => {
          let tabTitle = block.attributes.title;
          if (block.clientId === tabClientId) {
            tabTitle = content;
          }
          return {
            clientId: block.clientId,
            title: tabTitle
          };
        });
        //console.log(newTabs);
        setAttributes({ tabs: newTabs });
      },
      onTabDelete: clientId => {
        dispatch("core/block-editor").removeBlock(clientId);
      }
    };
  })
])(ParentEdit);
