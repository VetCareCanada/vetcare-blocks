import { Component } from "@wordpress/element";
import classnames from "classnames";
import { InnerBlocks } from "@wordpress/block-editor";
import slugify from "slugify";

class Edit extends Component {
  render() {
    const { attributes, setAttributes, className, clientId } = this.props;

    const { title } = attributes;
    const tabSlug = slugify(title);
    //console.log(this.props);

    // Set the block identifier
    setAttributes({ clientId: clientId });

    const classes = classnames({
      "vc-tab": true,
      [className]: !!className,
      "tab-pane": true,
      fade: true,
      "active show": tabSlug === "Overview"
    });
    return (
      <div
        className={classes}
        id={`vcTabPane-${tabSlug}`}
        role="tabpanel"
        aria-labelledby={`vcTab-${tabSlug}`}
      >
        <InnerBlocks template={[["core/paragraph"]]} />
      </div>
    );
  }
}

export default Edit;
