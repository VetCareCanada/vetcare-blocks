import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import ParentEdit from "./parent-edit";
import ParentSave from "./parent-save";

const attributes = {
  clientId: {
    type: "string"
  },
  tabs: {
    type: "array",
    default: []
  }
};

registerBlockType("vc-blocks/tabs", {
  title: __("Tabs", "vc-blocks"),
  description: __("Tabs", "vc-blocks"),
  icon: "grid-view",
  category: "vc-category",
  supports: {
    html: false,
    align: ["wide", "full"]
  },

  keywords: [__("tabs", "vc-blocks"), __("Tab", "vc-blocks")],
  attributes,
  edit: ParentEdit,
  save: ParentSave
});
