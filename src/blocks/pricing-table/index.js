import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";
import icon from "./icon";

const attributes = {
  count: {
    type: "number",
    default: 2
  }
};

registerBlockType("vc-blocks/pricing-table", {
  title: __("Pricing table", "vc-blocks"),
  description: __("Pricing table", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Pricing table", "vc-blocks"),
    __("pricing plans", "vc-blocks")
  ],
  icon: icon,
  attributes,
  edit,
  save
});
