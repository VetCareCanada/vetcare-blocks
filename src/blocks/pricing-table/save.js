import { InnerBlocks } from "@wordpress/block-editor";
import classnames from "classnames";

function save({ attributes }) {
  const { count } = attributes;
  const classes = classnames({
    [`has-${count}-columns`]: true
  });
  return (
    <div className={classes}>
      <InnerBlocks.Content />
    </div>
  );
}

export default save;
