import "./style.editor.scss";
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";
import icon from "../icon";

const attributes = {
  title: {
    type: "string",
    source: "html",
    selector: ".pricing-table__title"
  },
  price: {
    type: "string",
    source: "html",
    selector: ".pricing-table__price"
  },
  specialPrice: {
    type: "string",
    source: "html",
    selector: ".pricing-table__specialPrice"
  },
  features: {
    type: "string",
    source: "html",
    selector: ".pricing-table__features"
  },
  showPricingHeader: {
    type: "boolean",
    default: true
  },
  backgroundColor: {
    type: "string"
    //default: "primary"
  },
  textColor: {
    type: "string"
    //default: "white"
  },
  customBackgroundColor: {
    type: "string"
  },
  customTextColor: {
    type: "string"
  }
};

registerBlockType("vc-blocks/pricing-table-item", {
  title: __("Pricing table Item", "vc-blocks"),
  description: __("Pricing table", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Pricing table", "vc-blocks"),
    __("pricing plans", "vc-blocks")
  ],
  icon: icon,
  parent: ["vc-blocks/pricing-table"],
  attributes,
  edit,
  save
});
