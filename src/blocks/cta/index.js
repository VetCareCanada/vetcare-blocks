import "./style.editor.scss";
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import Edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/cta", {
    title: __("Call to action", "vc-blocks"),
    description: __("Call to action section", "vc-blocks"),
    category: "vc-category",
    keywords: [__("call to action", "vc-blocks"), __("cta", "vc-blocks")],
    supports: {
        className: false,
        align: ["wide", "full"]
    },
    attributes: {
        align: {
            type: "string",
            default: "wide"
        },
        content: {
            type: "string",
            source: "html",
            selector: "h2"
        },
        backgroundColor: {
            type: "string"
            //default: "primary"
        },
        textColor: {
            type: "string"
            //default: "white"
        },
        customBackgroundColor: {
            type: "string"
        },
        customTextColor: {
            type: "string"
        }
    },
    edit: Edit,
    save: save
});
