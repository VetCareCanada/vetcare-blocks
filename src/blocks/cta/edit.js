import {Component} from "@wordpress/element";
import {
    ContrastChecker,
    PanelColorSettings,
    RichText,
    InspectorControls,
    withColors, InnerBlocks
} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import classnames from "classnames";

class Edit extends Component {
    editorColors = wp.data.select("core/editor").getEditorSettings().colors;
    state = {
        currentButtonColor: null
    };

    onContentChange = content => {
        this.props.setAttributes({content});
    };

    onButtonToggle = value => {
        this.props.setAttributes({isExternal: value});
    };


    render()
    {
        const {
            attributes,
            backgroundColor,
            textColor,
            setBackgroundColor,
            setTextColor,
        } = this.props;
        const {
            content,
            className
        } = attributes;

        const ALLOWED_BLOCKS = ["core/button"];
        const TEMPLATE = [
            ["core/button", {url: "#", text: "button text"}]
        ];
        const classes = classnames({
            cta: true,
            [className]: !!className
        });

        return (
            <>
                <InspectorControls>
                    <PanelColorSettings
                        title={__("Color Settings", "vc-blocks")}
                        initialOpen={false}
                        colorSettings={[
                            {
                                value: backgroundColor.color,
                                onChange: setBackgroundColor,
                                label: __("Background Color", "vc-blocks")
                            },
                            {
                                value: textColor.color,
                                onChange: setTextColor,
                                label: __("Text Color", "vc-blocks")
                            }
                            ]}
                    >
                        <ContrastChecker
                            textColor={textColor.color}
                            backgroundColor={backgroundColor.color}
                        />
                    </PanelColorSettings>
                </InspectorControls>
                <div
                    className={classes}
                    style={{
                        backgroundColor: backgroundColor.color,
                        color: textColor.color
                    }}
                >
                    <div className="container">
                        <div className="cta__content">
                            <RichText
                                tagName="h2"
                                value={content}
                                style={{color: textColor.color}}
                                onChange={this.onContentChange}
                                placeholder={__("Add some text here...", "vc-blocks")}
                            />
                            <InnerBlocks
                                allowedBlocks={ALLOWED_BLOCKS}
                                template={TEMPLATE}
                                templateLock="all"
                            />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default withColors("backgroundColor", {textColor: "color"})(Edit);
