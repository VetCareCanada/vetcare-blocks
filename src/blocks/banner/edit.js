/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import {BlockControls, InnerBlocks, MediaPlaceholder, MediaUpload, MediaUploadCheck} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {withSelect} from "@wordpress/data";


/**
 * Local dependencies
 */
import Inspector from "./inspector";
import DesktopLayout from './layouts/desktop';
import MobileLayout from './layouts/mobile';
import TabletLayout from './layouts/tablet';
import BlockToolbar from "./toolbar";

const ALLOWED_BLOCKS = ['core/paragraph', 'core/heading', 'core/buttons', 'core/image'];
const TEMPLATE = [['core/heading']];

class Edit extends Component {
    
    render(){
        const { attributes, isSelected } = this.props;
        const { desktopImage, tabletImage, mobileImage, currentView, includeTextOverlay } = attributes;
        return (
            <div className='vc-banner-block-edit'>
                {isSelected && desktopImage.url !=='' && <Inspector {...this.props} />}
                <div className="vc-banner-block__image">
                    {currentView === 'desktop' && <DesktopLayout {...this.props} />}
                    {currentView === 'tablet' && <TabletLayout {...this.props} />}
                    {currentView === 'mobile' && <MobileLayout {...this.props} />}
                </div>
                {includeTextOverlay &&
                    <div className="vc-banner-block__content">
                        <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE}/>
                    </div>
                }
                
            </div>
        )
    }
}

export default Edit;