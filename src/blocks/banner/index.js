import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
    desktopImage: {
        type: 'object',
        default: {
            id: '',
            alt: '',
            url: ''
        }
    },
    tabletImage: {
        type: 'object',
        default: {
            id: '',
            alt: '',
            url: ''
        }
    },
    mobileImage: {
        type: 'object',
        default: {
            id: '',
            alt: '',
            url: ''
        }
    },
    includeTextOverlay: {
        type: 'boolean',
        default: true
    },
    currentView: {
        type: 'string',
        default: 'desktop'
    }
}

registerBlockType("vc-blocks/banner", {
    title: __("Banner", "vc-blocks"),
    description: __(
        "Block to allow different image on mobile",
        "vc-blocks"
    ),
    attributes,
    category: "vc-category",
    keywords: [__("image", "vc-blocks"), __("banner", "vc-blocks")],
    icon: "dashicons-cover-image",
    edit,
    save
})