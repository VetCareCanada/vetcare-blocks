/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import { InnerBlocks } from "@wordpress/block-editor";

class Save extends Component {
    render(){
        const { attributes } = this.props;
        const { desktopImage, tabletImage, mobileImage, includeTextOverlay } = attributes;
        return (
            <div className='vc-banner-block'>
                <picture className='vc-banner-block__image'>
                {mobileImage && mobileImage.url !== '' &&
                    <source 
                        media='(max-width: 480px)'
                        srcSet={mobileImage.url}
                        alt={mobileImage.alt}
                    />
                }
                {tabletImage && tabletImage.url !== '' &&
                    <source 
                        media='(max-width: 768px)'
                        srcSet={tabletImage.url}
                        alt={tabletImage.alt}
                    />
                }
                {desktopImage && desktopImage.url !== '' &&
                    <img 
                        src={desktopImage.url} 
                        alt={desktopImage.alt}
                    />
                }
                </picture>
                {includeTextOverlay && 
                    <div className="vc-banner-block__content">
                        <InnerBlocks.Content />
                    </div>
                }
            </div>
        )
    }
}

export default Save;