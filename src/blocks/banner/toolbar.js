/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import {BlockControls, InnerBlocks, MediaPlaceholder, MediaUpload, MediaUploadCheck} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {withSelect} from "@wordpress/data";

class BlockToolbar extends Component {
    onSelectDesktopImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            desktopImage: {
                id,
                alt,
                url
            }
         })
    }

    onSelectTabletImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            tabletImage: {
                id,
                alt,
                url
            }
         })
    }

    onSelectMobileImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            mobileImage: {
                id,
                alt,
                url
            }
         })
    }


    removeDesktopImage = () => {
        this.props.setAttributes({ 
            desktopImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }

    removeTabletImage = () => {
        this.props.setAttributes({ 
            tabletImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }

    removeMobileImage = () => {
        this.props.setAttributes({ 
            mobileImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }

    render(){
        const { attributes } = this.props;
        const { desktopImage, tabletImage, mobileImage, currentView } = attributes;
        console.log(desktopImage.alt);
        
        return (
            <Toolbar>
                    {currentView === 'desktop' &&
                    <MediaUploadCheck>
                         <MediaUpload
                            onSelect={this.onSelectDesktopImage}
                            allowedTypes={["image"]}
                            value={desktopImage.id}
                            render={({open}) => {
                            return (
                                <IconButton
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Add/Edit desktop Image", "vc-blocks")}
                                    onClick={open}
                                    icon="format-image"
                                />
                            );
                        }}
                    />
                    {desktopImage.id && (
                        <IconButton
                            className="components-icon-button components-toolbar__control"
                            label={__("Remove Background Image", "vc-blocks")}
                            onClick={this.removeDesktopImage}
                            icon="trash"
                        />
                    )}
                    </MediaUploadCheck>
                    }


                    {currentView === 'tablet' &&
                    <MediaUploadCheck>
                         <MediaUpload
                            onSelect={this.onSelectTabletImage}
                            allowedTypes={["image"]}
                            value={tabletImage.id}
                            render={({open}) => {
                            return (
                                <IconButton
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Add/Edit tablet Image", "vc-blocks")}
                                    onClick={open}
                                    icon="format-image"
                                />
                            );
                        }}
                    />
                    {tabletImage.id && (
                        <IconButton
                            className="components-icon-button components-toolbar__control"
                            label={__("Remove Background Image", "vc-blocks")}
                            onClick={this.removeTabletImage}
                            icon="trash"
                        />
                    )}
                    </MediaUploadCheck>
                    }

                    {currentView === 'mobile' &&
                    <MediaUploadCheck>
                         <MediaUpload
                            onSelect={this.onSelectMobileImage}
                            allowedTypes={["image"]}
                            value={mobileImage.id}
                            render={({open}) => {
                            return (
                                <IconButton
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Add/Edit mobile Image", "vc-blocks")}
                                    onClick={open}
                                    icon="format-image"
                                />
                            );
                        }}
                    />
                    {tabletImage.id && (
                        <IconButton
                            className="components-icon-button components-toolbar__control"
                            label={__("Remove Background Image", "vc-blocks")}
                            onClick={this.removeMobileImage}
                            icon="trash"
                        />
                    )}
                    </MediaUploadCheck>
                    }
                    
                
            </Toolbar>
        )
    }
}

export default BlockToolbar;