<?php
$swiper_testimonials =
	'<div class="' . $classes . '">
        <div class="vc-testimonial-list">';

while ( $slides_query->have_posts() ) {
	$slides_query->the_post();
	if ( $attributes['excerpt'] && true === $attributes['excerpt'] ) {
		$content = get_the_excerpt();
	} else {
		$content = get_the_content();
	}
	$swiper_testimonials .=
	  '<div class="vc-testimonial-list-item">

            <div class="vc-testimonial-list__content">
                ' . $content . '
            </div>
            <div class="vc-testimonial-list__title">
                ' . get_the_title() . '
            </div>
        </div>';
}
$swiper_testimonials .= '</div>';
$swiper_testimonials .= '</div>';
