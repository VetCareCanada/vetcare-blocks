import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";

registerBlockType("vc-blocks/swiper-testimonials", {
  title: __("Swiper Testimonials", "vc-blocks"),
  description: __("Swiper Testimonials", "vc-blocks"),
  category: "vc-category",
  keywords: [__("Swiper", "vc-blocks"), __("Testimonials", "vc-blocks")],
  icon: "wordpress",
  edit,
  save() {
    return null;
  }
});
