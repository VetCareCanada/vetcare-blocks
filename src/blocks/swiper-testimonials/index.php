<?php
/**
 * Serverside Rendering file for Yoast Breadcrumbs Block
 */

?>
<?php

/**
 * Render Callback function for Yoast Breadcrumbs Blocks
 *
 * @param array $attributes Gutenberg Block attributes registered in register_block() in main plugin.php.
 *
 * @return string
 */
function vc_blocks_render_swiper_testimonials(array $attributes): string
{
    //print_r($attributes);
    $classes = 'wp-block-vc-blocks-testimonials ';
    if (isset($attributes['className'])) {
        $classes .= $attributes['className'];
    }
    $args = array(
        'posts_per_page' => $attributes['sliderFetchCount'] ? $attributes['sliderFetchCount'] : 5,
        'post_type'      => $attributes['postType'] ? $attributes['postType'] : 'vc_testimonial',
    );

    $slides_query        = new WP_Query($args);
    $swiper_testimonials = '';
    if ($slides_query->have_posts()) {
        if ('list' === $attributes['layoutType']) {
            include plugin_dir_path(__FILE__) . 'layouts/list.php';
        } else {
            include plugin_dir_path(__FILE__) . 'layouts/slider.php';
        }
        wp_reset_postdata();
        return $swiper_testimonials;
    } else {
        return '<div>' . __('No Posts Found', 'vc-blocks') . '</div>';
    }
}
