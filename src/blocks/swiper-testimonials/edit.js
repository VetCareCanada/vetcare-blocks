/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import ServerSideRender from "@wordpress/server-side-render";
/**
 * Internal dependencies
 */
import Inspector from "./inspector";
import {withSelect} from "@wordpress/data";

class Edit extends Component {
  render() {
    const { className, isSelected } = this.props;
    console.log(this.props);
    const classes = classnames({
      [className]: !!className
    });
    return (
      <Fragment>
        {isSelected && <Inspector {...this.props}/>}
        <ServerSideRender
          block="vc-blocks/swiper-testimonials"
          attributes={ this.props.attributes }
        />
      </Fragment>

    );
  }
}

export default withSelect((select, props) => {
  const { attributes } = props;
  const { sliderCount, postType } = attributes;
  let query = { per_page: sliderCount };

  return {
    posts: select("core").getEntityRecords("postType", postType, query),
    postTypes: select("core").getPostTypes()
  };
})(Edit);
