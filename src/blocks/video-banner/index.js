/**
 * Wordpress Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import {video as icon} from '@wordpress/icons';

/**
 * Internal Dependencies
 */
import "./style.scss";
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/video-banner", {
  title: __("Video Banner", "vc-blocks"),
  description: __("Video Banner", "vc-blocks"),
  category: "vc-category",
  keywords: [__("Video", "vc-blocks"), __("Video Banner", "vc-blocks")],
  icon: icon,
  attributes: {
    videoUrl: {
      type: "string",
      src: "attribute",
      attribute: "data-youtube"
    },
    minHeight: {
      type: "string",
      default: "600px"
    },
    overlayColor: {
      type: "string"
    },
    customOverlayColor: {
      type: "string"
    },
    dimRatio: {
      type: "number",
      default: 50
    },
    gradient: {
      "type": "string"
    },
    customGradient: {
      "type": "string"
    },
    align: {
      type: 'string',
      default: 'full'
    }
  },
  supports: {
    align: ["full"]
  },
  edit,
  save
});
