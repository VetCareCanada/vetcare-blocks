import classnames from "classnames";
import { getColorClassName, InnerBlocks } from "@wordpress/block-editor";

const save = ({ attributes }) => {
    console.log(attributes);
    const {
        sameHeight,
        mediaId,
        mediaAlt,
        mediaUrl,
        mediaHorizontalAlignment,
        mediaVerticalAlignment,
        textColor,
        customTextColor,
        backgroundColor,
        customBackgroundColor,
        //backgroundImageId,
        backgroundImageUrl
    } = attributes;
    const backgroundClass = getColorClassName(
        "background-color",
        backgroundColor
    );
  const textClass = getColorClassName("color", textColor);

  const classes = classnames({
        "img-text": true,
        "img-text--sameHeight": sameHeight,
        [backgroundClass]: backgroundClass,
        [textClass]: textClass
    });
  const innerClasses = classnames({
        "img-text__inner": true,
        "align-items-start": !sameHeight && mediaVerticalAlignment === "top",
        "align-items-center": !sameHeight && mediaVerticalAlignment === "center",
        "align-items-end": !sameHeight && mediaVerticalAlignment === "bottom"
    });
  const mediaClasses = classnames({
        "img-text__media": true,
        "order-md-2 text-md-right": mediaHorizontalAlignment === "right"
    });
  const contentClasses = classnames({
        "img-text__content": true,
        "justify-content-start": sameHeight && mediaVerticalAlignment === "top",
        "justify-content-center": sameHeight && mediaVerticalAlignment === "center",
        "justify-content-end": sameHeight && mediaVerticalAlignment === "bottom"
    });
  const contentStyles = {};
  const styles = {
        color: textClass ? undefined : customTextColor
    };
    if (sameHeight) {
        contentStyles.backgroundColor = backgroundClass
        ? undefined
        : customBackgroundColor;
    } else {
        styles.backgroundColor = backgroundClass
        ? undefined
        : customBackgroundColor;
    }
    if (backgroundImageUrl) {
        if (sameHeight) {
            contentStyles.backgroundImage = `url(${backgroundImageUrl})`;
        } else {
            styles.backgroundImage = `url(${backgroundImageUrl})`;
        }
    }
    return (
    <div className={classes} style={styles}>
      <div className={innerClasses}>
        <div className={mediaClasses}>
          <img src={mediaUrl} alt={mediaAlt} className={mediaId ? `wp-image-${mediaId}` : null}/>
        </div>
        <div className={contentClasses} style={contentStyles}>
          <InnerBlocks.Content />
        </div>
      </div>
    </div>
  );
};

export default save;
