import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/img-text", {
  title: __("Image & Text", "vc-blocks"),
  description: __("Image & Text", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Image & Text", "vc-blocks"),
    __("Image", "vc-blocks"),
    __("Img", "vc-blocks"),
    __("Text", "vc-blocks")
  ],
  supports: {
    align: ["full"],
  },
  icon: (
    <svg
      width="24"
      height="24"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      role="img"
      aria-hidden="true"
      focusable="false"
    >
      <path d="M13 17h8v-2h-8v2zM3 19h8V5H3v14zM13 9h8V7h-8v2zm0 4h8v-2h-8v2z"></path>
    </svg>
  ),
  attributes: {
    sameHeight: {
      type: "boolean",
      default: false
    },
    mediaId: {
      type: "number"
    },
    mediaAlt: {
      type: "string",
      source: "attribute",
      selector: "img",
      attribute: "alt"
    },
    mediaUrl: {
      type: "string",
      source: "attribute",
      selector: "img",
      attribute: "src"
    },
    mediaHorizontalAlignment: {
      type: "string",
      default: "left"
    },
    mediaVerticalAlignment: {
      type: "string",
      default: "top"
    },
    backgroundColor: {
      type: "string"
    },
    textColor: {
      type: "string"
    },
    customBackgroundColor: {
      type: "string"
    },
    customTextColor: {
      type: "string"
    },
    backgroundImageId: {
      type: "number"
    },
    backgroundImageUrl: {
      type: "string"
    }
  },
  edit,
  save
});
