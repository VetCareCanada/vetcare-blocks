/**
 * Internal dependencies
 */
import "./style.editor.scss";
import Edit from "./edit";
import save from "./save";

/**
 * Wordpress dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType("vc-blocks/fancy-heading", {
  title: "Fancy Heading",
  description: "Fancy Heading with optional link button",
  category: "vc-category",
  keywords: [
    __("Fancy heading", "vc-blocks"),
    __("Heading", "vc-blocks"),
    __("Title", "vc-blocks")
  ],
  supports: {
    className: false,
    align: ["left", "right", "center", "full"],
    "anchor": true
  },
  attributes: {
    heading1: {
      type: "string",
      source: "html",
      selector: "p"
    },
    heading2: {
      type: "string",
      source: "html",
      selector: "h2"
    },
    hasButton: {
      type: "boolean",
      default: false
    }
  },
  edit: Edit,
  save: save
});
