/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Internal dependencies
 */
import Inspector from "./inspector";

/**
 * WordPress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import { RichText, InnerBlocks } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";

const ALLOWED_BLOCKS = ["core/button"];
const BUTTONS_TEMPLATE = [["core/button", { text: "Learn more", url: "#" }]];

class Edit extends Component {
  onHeading1Change = heading1 => {
    this.props.setAttributes({ heading1 });
  };
  onHeading2Change = heading2 => {
    this.props.setAttributes({ heading2 });
  };

  render() {
    const { attributes, isSelected } = this.props;
    const { heading1, heading2, hasButton, className } = attributes;

    const classes = classnames({
      "fancy-heading": true,
      "has-button": !!hasButton,
      [className]: !!className
    });

    return (
      <Fragment>
        {isSelected && <Inspector {...this.props} />}
        <div className={classes}>
          <div className="left">
            <RichText
              tagName="p"
              value={heading1}
              onChange={this.onHeading1Change}
              placeholder={__("heading 1", "vc-blocks")}
            />
            <RichText
              tagName="h2"
              value={heading2}
              onChange={this.onHeading2Change}
              placeholder={__("heading 2", "vc-blocks")}
            />
          </div>

          {hasButton && (
            <InnerBlocks
              allowedBlocks={ALLOWED_BLOCKS}
              template={BUTTONS_TEMPLATE}
              templateLock="insert"
            />
          )}
        </div>
      </Fragment>
    );
  }
}

export default Edit;
