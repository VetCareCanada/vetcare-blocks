/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Wordpress dependencies
 */
import { RichText, InnerBlocks } from "@wordpress/block-editor";

function save({ attributes }) {
  const { heading1, heading2, hasButton } = attributes;

  const classes = classnames({
    "fancy-heading": true,
    "has-button": !!hasButton
  });
  return (
    <div className={classes}>
      <div className="left">
        <RichText.Content tagName="p" value={heading1} />

        <RichText.Content tagName="h2" value={heading2} />
      </div>

      {hasButton && <InnerBlocks.Content />}
    </div>
  );
}

export default save;
