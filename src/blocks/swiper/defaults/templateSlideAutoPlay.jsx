export const TEMPLATE_AUTOPLAY = {
  delay: 3000,
  stopOnLastSlide: false,
  disableOnInteraction: true,
  reverseDirection: false,
  waitForTransition: true
};
