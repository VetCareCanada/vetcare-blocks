export const TEMPLATE_SLIDE = {
  type: "div",
  props: {
    class: "swiper-slide",
    children: [
      {
        type: "img",
        props: {
          src: "",
          alt: "",
          dataId: "",
          children: []
        }
      }
    ]
  }
};
