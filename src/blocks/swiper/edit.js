import { Component } from "@wordpress/element";
import { MediaPlaceholder } from "@wordpress/block-editor";
import { cloneDeep, clone } from "lodash";

import Swiper from "swiper";

import "./style.editor.scss";
import "swiper/css/swiper.min.css";

import SwiperInspectorControls from "./components/SwiperInspectorControls";
import { TEMPLATE_SLIDE } from "./defaults/templateSlide.jsx";
import SwiperBlockControls from "./components/SwiperBlockControls";
class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = { currentSlide: 0 };
  }

  sliderHandle = null;

  initSlider = sliderConfig => {
    const blockSlider = new Swiper(".swiper-container", {
      direction: "horizontal",
      loop: false,
      slidesPerView: sliderConfig.slidesPerView,
      spaceBetween: sliderConfig.spaceBetween,
      initialSlide: sliderConfig.initialSlide,
      allowTouchMove: false,
      centeredSlides: true,
      centerInsufficientSlides: true
    });
    return blockSlider;
  };

  destroySlider = slider => {
    // @params
    // slider.destroy(deleteInstance, cleanStyles)
    // ref https://swiperjs.com/api/
    slider.destroy(true, true);
    //console.log("Destroyed Slider");
  };

  render = () => {
    const {
      clientId,
      attributes,
      setAttributes,
      className,
      noticeUI
    } = this.props;

    const { slideData, sliderConfig } = attributes;

    //console.log(attributes);

    this.componentDidMount = () => {
      this.initSlider(sliderConfig);
    };

    this.sliderHandle = this.initSlider(sliderConfig); // Initial Init with SliderConfig from attributes

    this.sliderHandle.on("slideChange", () => {
      // Method to stablilize redraws between swiper, wordpress and component level state to ensure that the swiper object and edit component are synced.
      this.setState({ currentSlide: this.sliderHandle.activeIndex });

      const newConfig = { ...sliderConfig };
      newConfig.initialSlide = this.sliderHandle.activeIndex;

      setAttributes({ sliderConfig: newConfig });
    });

    const renderSlides = slideData => {
      return slideData.map((slide, index) => {
        let { src, alt, dataId } = slide.props.children[0].props;
        return (
          <div className="swiper-slide" key={index}>
            {!src && (
              <MediaPlaceholder
                icon="format-image"
                onSelect={image => {
                  onSlideImageSelect(image, index);
                }}
                onSelectURL={() => {
                  //console.log("onSelectURL");
                }}
                onError={() => {
                  //console.log("onError");
                }}
                //accept="image/*"
                allowedTypes={["image"]}
                labels={{
                  title: "Slider Image",
                  instructions: "Upload Image to Slider"
                }}
                notices={noticeUI}
              />
            )}
            {src && (
              <>
                <img src={src} alt={alt} data-id={dataId} />
              </>
            )}
          </div>
        );
      });
    };

    const getSizesandSrcSet = image => {
      let sizesRef = image.sizes;
      let availableSizes = Object.keys(sizesRef).filter(
        e => e !== "thumbnail" && e !== "full"
      );
      let sizes = "";
      let srcset = "";
      availableSizes.map(size => {
        sizes = sizes.concat(
          `(max-width: ${Math.round(sizesRef[size].width)}px) ${Math.round(
            sizesRef[size].width
          )}px, `
        );
        srcset = srcset.concat(
          `${sizesRef[size].url} ${Math.round(sizesRef[size].width)}w, `
        );
      });
      sizes = sizes.concat(`${Math.round(sizesRef.full.width)}px`);
      srcset = srcset.concat(
        `${sizesRef.full.url} ${Math.round(sizesRef.full.width)}w`
      );
      let width = Math.round(sizesRef.full.width);

      return { sizes, srcset, width };
    };

    const createSlide = () => {
      let newData = [...slideData, TEMPLATE_SLIDE];
      setAttributes({ slideData: newData });
      this.sliderHandle.update();
      this.sliderHandle.updateSlides();
      serveNotice("info", "Slide Added");
    };

    const onSlideImageSelect = (image, slide) => {
      const currentSlidesData = cloneDeep(slideData);

      const { alt, url, id } = image;
      let sizesAndSrcSet = getSizesandSrcSet(image);

      const newSlide = clone({ ...slideData[slide] });

      const slideProps = newSlide.props.children[0].props;

      slideProps.src = url;
      slideProps.alt = alt;
      slideProps.dataId = id;
      slideProps.sizes = sizesAndSrcSet.sizes;
      slideProps.srcset = sizesAndSrcSet.srcset;
      slideProps.width = sizesAndSrcSet.width;

      newSlide.props.children[0].props = slideProps;

      const newSlideData = [...currentSlidesData];

      newSlideData[slide] = newSlide;

      setAttributes({ slideData: newSlideData });
    };

    const serveNotice = (type, message) => {
      wp.data.dispatch("core/notices").createNotice(
        type,
        message, // Text string to display.
        {
          isDismissible: true, // Whether the user can dismiss the notice.
          // Any actions the user can perform.
          type: "snackbar"
        }
      );
    };

    const SlideNextOrNotify = () => {
      let success = this.sliderHandle.slideNext();
      if (!success) {
        serveNotice("info", "No Next Slide");
      }
    };

    const SlidePrevOrNotify = () => {
      let success = this.sliderHandle.slidePrev();
      if (!success) {
        serveNotice("info", "No Previous Slide");
      }
    };

    return (
      <div className={className} data-identifier={clientId}>
        <SwiperInspectorControls
          attributes={attributes}
          setAttributes={setAttributes}
        />
        <SwiperBlockControls
          attributes={attributes}
          setAttributes={setAttributes}
          sliderHandle={this.sliderHandle}
          getSizesandSrcSet={getSizesandSrcSet}
          createSlide={createSlide}
          initSlider={this.initSlider}
          SlideNextOrNotify={SlideNextOrNotify}
          SlidePrevOrNotify={SlidePrevOrNotify}
        />
        <div className="swiper-container">
          <div className="swiper-wrapper">{renderSlides(slideData)}</div>
        </div>
      </div>
    );
  };
}

export default Edit;
