import { InspectorControls } from "@wordpress/block-editor";
import {
  PanelBody,
  ToggleControl,
  RangeControl,
  TextControl,
  ColorPalette
} from "@wordpress/components";
import { __ } from "@wordpress/i18n";

import { TEMPLATE_BREAKPOINTS } from "../defaults/templateSwiperBreakpoints.jsx";

const SwiperInspectorControls = props => {
  const { attributes, setAttributes } = props;

  const { sliderConfig } = attributes;
  const { xs, sm, md, lg, xl } = sliderConfig.responsive;
  const onSwiperConfigChange = (config, setting, value) => {
    const newConfig = { ...config, [setting]: value };
    //console.log(newConfig);
    setAttributes({ sliderConfig: newConfig });
  };

  const onSwiperNestedConfigChange = (
    config = {},
    subSetting = "",
    setting = "",
    value = ""
  ) => {
    let newConfig = { ...config };

    newConfig[subSetting][setting] = value;
    //console.log(newConfig);
    setAttributes({ sliderConfig: newConfig });
  };

  const onSwiperResponsiveSettingChange = (
    configObject = {},
    breakpointObject = {},
    breakpointName = "",
    attribute = "",
    value = ""
  ) => {
    //console.log("START CONFIG CHANGE");
    //console.log("Config Object:");
    //console.log(configObject);
    //console.log("Breakpoint Object:");
    //console.log(breakpointObject);
    //console.log("Breakpoint Name: " + breakpointName);
    //console.log("Setting Name: " + attribute);
    //console.log("Setting Value: " + value);

    //Add / Modify attribute of breakpoint object
    const newBreakpointObject = { ...breakpointObject, [attribute]: value };
    // Add Modify breakpoint object in responsive object of configObject
    const newResponsiveObject = {
      ...configObject.responsive,
      [breakpointName]: newBreakpointObject
    };
    // Modify Responsive Object in Config Object
    const newConfigObject = {
      ...configObject,
      responsive: { ...newResponsiveObject }
    };

    //console.log(newConfigObject);
    //console.log("END CONFIG CHANGE");

    setAttributes({ sliderConfig: newConfigObject });
  };

  const onSwiperResetResponsive = () => {
    const newResponsiveObject = TEMPLATE_BREAKPOINTS;
    const newConfigObject = {
      ...sliderConfig,
      responsive: { ...newResponsiveObject }
    };
    newConfigObject.defaultResponsive = true;

    setAttributes({ sliderConfig: newConfigObject });
  };

  return (
    <InspectorControls>
      <PanelBody title={__("Color Options", "vc-blocks")}>
        <ColorPalette
          title={__("Navigation Color", "vc-blocks")}
          colors={wp.data.select("core/editor").getEditorSettings().colors}
          onChange={color => {
            onSwiperConfigChange(sliderConfig, "navColor", color);
          }}
          value={sliderConfig.navColor}
        />
      </PanelBody>

      <PanelBody title={__("Swiper Options", "vc-blocks")}>
        <RangeControl
          label={__(" Max Slides to Show", "vc-blocks")}
          onChange={value =>
            onSwiperConfigChange(sliderConfig, "slidesPerView", value)
          }
          value={sliderConfig.slidesPerView}
          min="1"
          max="10"
          step="1"
        />
        <ToggleControl
          label={__("Loop Slides", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "loop", value);
          }}
          checked={sliderConfig.loop}
        />
        <ToggleControl
          label={__("Auto Slide Height", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "autoHeight", value);
          }}
          checked={sliderConfig.autoHeight}
        />

        <ToggleControl
          label={__("Arrow Buttons?", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "hasArrows", value);
          }}
          checked={sliderConfig.hasArrows}
        />

        <ToggleControl
          label={__("Dot Pagination", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "hasDots", value);
          }}
          checked={sliderConfig.hasDots}
        />

        <ToggleControl
          label={__("Centered Mode (Frontend Only)", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "centeredSlides", value);
          }}
          checked={sliderConfig.centeredSlides}
        />

        <ToggleControl
          label={__("Autoplay (Frontend Only)", "vc-blocks")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "doAutoPlay", value);
          }}
          checked={sliderConfig.doAutoPlay}
        />
        {sliderConfig.doAutoPlay && (
          <RangeControl
            label={__("AutoPlay Delay (ms)", "vc-blocks")}
            onChange={value => {
              onSwiperNestedConfigChange(
                sliderConfig,
                "autoplay",
                "delay",
                value
              );
            }}
            value={sliderConfig.autoplay.delay}
            min="300"
            max="6000"
            step="100"
          />
        )}
        <ToggleControl
          label={__("Default Responsive Sizes?", "vc-blocks")}
          onChange={value => {
            if (value === true) {
              //console.log("Reset Responsive Breakpoints.");
              onSwiperResetResponsive();
            } else {
              onSwiperConfigChange(sliderConfig, "defaultResponsive", value);
            }
          }}
          checked={sliderConfig.defaultResponsive}
        />
        {!sliderConfig.defaultResponsive && (
          <>
            <h2
              className="components-panel__body-title"
              style={{ padding: "16px" }}
            >
              Custom Breakpoints
            </h2>
            <RangeControl
              label={__("XS Slides", "vc-blocks")}
              onChange={value => {
                onSwiperResponsiveSettingChange(
                  sliderConfig,
                  sliderConfig.responsive.xs,
                  "xs",
                  "slidesPerView",
                  value
                );
              }}
              value={
                xs.slidesPerView < sliderConfig.slidesPerView
                  ? xs.slidesPerView
                  : sliderConfig.slidesPerView
              }
              min="1"
              max={sliderConfig.slidesPerView}
              step="1"
            />
            <RangeControl
              label={__("SM Slides", "vc-blocks")}
              onChange={value => {
                onSwiperResponsiveSettingChange(
                  sliderConfig,
                  sliderConfig.responsive.sm,
                  "sm",
                  "slidesPerView",
                  value
                );
              }}
              value={
                sm.slidesPerView < sliderConfig.slidesPerView
                  ? sm.slidesPerView
                  : sliderConfig.slidesPerView
              }
              min="1"
              max={sliderConfig.slidesPerView}
              step="1"
            />
            <RangeControl
              label={__("MD Slides", "vc-blocks")}
              onChange={value => {
                onSwiperResponsiveSettingChange(
                  sliderConfig,
                  sliderConfig.responsive.md,
                  "md",
                  "slidesPerView",
                  value
                );
              }}
              value={
                md.slidesPerView < sliderConfig.slidesPerView
                  ? md.slidesPerView
                  : sliderConfig.slidesPerView
              }
              min="1"
              max={sliderConfig.slidesPerView}
              step="1"
            />
            <RangeControl
              label={__("LG Slides", "vc-blocks")}
              onChange={value => {
                onSwiperResponsiveSettingChange(
                  sliderConfig,
                  sliderConfig.responsive.lg,
                  "lg",
                  "slidesPerView",
                  value
                );
              }}
              value={
                lg.slidesPerView < sliderConfig.slidesPerView
                  ? lg.slidesPerView
                  : sliderConfig.slidesPerView
              }
              min="1"
              max={sliderConfig.slidesPerView}
              step="1"
            />
            <RangeControl
              label={__("XL Slides", "vc-blocks")}
              onChange={value => {
                onSwiperResponsiveSettingChange(
                  sliderConfig,
                  sliderConfig.responsive.xl,
                  "xl",
                  "slidesPerView",
                  value
                );
              }}
              value={
                xl.slidesPerView < sliderConfig.slidesPerView
                  ? xl.slidesPerView
                  : sliderConfig.slidesPerView
              }
              min="1"
              max={sliderConfig.slidesPerView}
              step="1"
            />
          </>
        )}
        <TextControl
          label={__("Space Between Slides (px)")}
          onChange={value => {
            onSwiperConfigChange(sliderConfig, "spaceBetween", value);
          }}
          value={sliderConfig.spaceBetween}
          type="number"
        />
      </PanelBody>
    </InspectorControls>
  );
};

export default SwiperInspectorControls;
