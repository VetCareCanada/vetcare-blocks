import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import edit from "./edit";
import save from "./save";

import { TEMPLATE_SLIDE } from "./defaults/templateSlide.jsx";
import { TEMPLATE_BREAKPOINTS } from "./defaults/templateSwiperBreakpoints.jsx";
import { TEMPLATE_AUTOPLAY } from "./defaults/templateSlideAutoPlay.jsx";

const attributes = {
  slideData: {
    type: "array",
    source: "children",
    selector: ".swiper-wrapper",
    default: [TEMPLATE_SLIDE] // @object -- Reference ./defaults/templateSlide.jsx
  },
  sliderConfig: {
    type: "object",
    default: {
      slidesPerView: 1,
      loop: true,
      defaultResponsive: true,
      spaceBetween: 15,
      initialSlide: 0,
      centeredSlides: true,
      autoHeight: true,
      doAutoPlay: false,
      hasArrows: true,
      hasDots: true,
      navColor: "##FF0000",
      responsive: TEMPLATE_BREAKPOINTS, // @object  -- Reference ./defaults/templateSwiperBreakpoints.jsx
      autoplay: TEMPLATE_AUTOPLAY // @object -- Reference ./defaults/templateSwiperAutoPlay.jsx,
    }
  }
};

registerBlockType("vc-blocks/swiper", {
  title: __("Swiper", "vc-blocks"),
  description: __("Block for Swiper layouts", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Slider", "vc-blocks"),
    __("carousel", "vc-blocks"),
    __("swiper", "vc-blocks")
  ],
  icon: "slides",
  attributes,
  edit,
  save
});
