import Swiper from "swiper";

let swiperContainer = document.querySelector(".swiper-container");

if (swiperContainer) {
  let swiperConfig = JSON.parse(swiperContainer.getAttribute("data-swiper-config"));
  const finalSettings = {...swiperConfig};
  finalSettings.autoplay = swiperConfig.doAutoPlay
    ? swiperConfig.autoplay
    : false;

  if (swiperConfig.doAutoPlay) {
    finalSettings.autoplay.delay = swiperConfig.autoplay.delay
      ? swiperConfig.autoplay.delay
      : 3000;
    finalSettings.autoplay.stopOnLastSlide = swiperConfig.autoplay
      .stopOnLastSlide
      ? swiperConfig.autoplay.stopOnLastSlide
      : false;
    finalSettings.autoplay.disableOnInteraction = swiperConfig.autoplay
      .disableOnInteraction
      ? swiperConfig.autoplay.disableOnInteraction
      : true;
    finalSettings.autoplay.reverseDirection = swiperConfig.autoplay
      .reverseDirection
      ? swiperConfig.autoplay.reverseDirection
      : false;
    finalSettings.autoplay.waitForTransition = swiperConfig.autoplay
      .waitForTransition
      ? swiperConfig.autoplay.waitForTransition
      : true;
  }

  finalSettings.breakpoints = {
    [swiperConfig.responsive.xs.width]: {
      slidesPerView: swiperConfig.responsive.xs.slidesPerView
    },
    [swiperConfig.responsive.sm.width]: {
      slidesPerView: swiperConfig.responsive.sm.slidesPerView
    },
    [swiperConfig.responsive.md.width]: {
      slidesPerView: swiperConfig.responsive.md.slidesPerView
    },
    [swiperConfig.responsive.lg.width]: {
      slidesPerView: swiperConfig.responsive.lg.slidesPerView
    },
    [swiperConfig.responsive.xl.width]: {
      slidesPerView: swiperConfig.responsive.xl.slidesPerView
    }
  };

  window.blockSlider = new Swiper(".swiper-container", {
    ...finalSettings, //settings from attributes modified by non-swiper options

    //overrides for frontend
    initialSlide: 0,

    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderBullet: (index, className) => {
        return `<span class="${className}" style="background-color:${finalSettings.navColor}"  ></span>`;
      }
    },
    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    setWrapperSize: true
  });
  //console.log(finalSettings);
}

