import { Component } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import { TextControl, PanelBody } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

const Inspector = props => {
    const { attributes } = props;
    const onChangeText = (setting, value) => {
        props.setAttributes({ [setting]: value })
    }
    return (
        <InspectorControls>
            <PanelBody title={__("MailChimp Data")}>
                <TextControl 
                    label="Form Action"
                    value={attributes.formAction}
                    onChange={(value) => {onChangeText("formAction", value)}}
                />
                <TextControl 
                    label="Bot Input Name"
                    value={attributes.botInputName}
                    onChange={(value) => {onChangeText("botInputName", value)}}
                />
                <TextControl 
                    label="Submit Text"
                    value={attributes.submitText}
                    onChange={(value) => {onChangeText("submitText", value)}}
                />
            </PanelBody>
        </InspectorControls>
    )
}

export default Inspector;