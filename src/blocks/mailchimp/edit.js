import { Component } from '@wordpress/element';
import Inspector from "./inspector";

class Edit extends Component {
    render(){
        const { isSelected, attributes } = this.props;
        const { formAction, botInputName, submitText } = attributes;
        return(
            <div className='vc-mailchimp-block'>
                {isSelected && <Inspector {...this.props} />}
                <div id="mc_embed_signup">
                <form action={formAction} method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                    <div id="mc_embed_signup_scroll">
                        <label htmlFor="mce-EMAIL">Subscribe</label>
                        <input type="email" value="" name="EMAIL" className="email" id="mce-EMAIL" placeholder="email address" required readOnly />

                        <div aria-hidden="true">
                            <input type="text" name={botInputName} tabIndex="-1" value="" readOnly />
                        </div>
                        <div className="clear">
                            <input type="submit" value={submitText} name="subscribe" id="mc-embedded-subscribe" className="button" readOnly />
                        </div>
                    </div>
                </form>
                </div>
            </div>
        )
    }
}

export default Edit;