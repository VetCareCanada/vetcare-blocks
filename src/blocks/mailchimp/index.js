import "./style.editor.scss";
import {registerBlockType} from '@wordpress/blocks';
import {__} from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
    formAction: {
        type: "string",
        default: ""
    },
    botInputName: {
        type: "string",
        default: ""
    },
    submitText: {
        type: "string",
        default: ""
    }
}

registerBlockType("vc-blocks/mailchimp", {
    title: __("Mailchimp", "vc-blocks"),
    description: __("Mailchimp/subscription", "vc-blocks"),
    category: "vc-category",
    keywords: [
        __("Mailchimp", "vc-blocks"),
        __("Subscription", "vc-blocks"),
        __("Newsletter", "vc-blocks")
    ],
    icon: "buddicons-pm",
    attributes,
    edit,
    save
})