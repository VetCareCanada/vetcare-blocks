import { Component } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import {
  PanelBody,
  SelectControl,
  ContrastChecker,
  PanelColorSettings
} from "@wordpress/components";
import { __ } from "@wordpress/i18n";

class Inspector extends Component {
  render() {
    const { attributes, posts, postTypes, taxonomies } = this.props;
    console.log(taxonomies)
    const onSliderSettingToggle = (setting, value) => {
      this.props.setAttributes({ [setting]: value });
    };

    const ALIGNMENT_OPTIONS = [
      {
        label: "Left",
        value: "tab-left"
      },
      {
        label: "Center",
        value: "tab-center"
      }
    ];

    const getTaxonomies = () => {
      if (!taxonomies) return [];
      let options = [];
      const restrictedTypes = ["page", "attachment", "wp_block"];
      taxonomies.map(taxonomy => {
        if (!restrictedTypes.includes(taxonomy.slug)) {
          options.push({
            label: taxonomy.name,
            value: taxonomy.slug
          });
        }
      });
      return options;
    };

    const getPostTypes = () => {
      if (!postTypes) return [];
      let options = [];
      const restrictedTypes = ["page", "attachment", "wp_block"];
      postTypes.map(postType => {
        if (!restrictedTypes.includes(postType.slug)) {
          options.push({
            label: postType.name,
            value: postType.slug
          });
        }
      });
      return options;
    };
    return (
      <InspectorControls>
        <PanelBody title={__("Post Type Options", "vc-blocks")}>
          <SelectControl
            label={__("Post Type?", "vc-blocks")}
            onChange={value => onSliderSettingToggle("postType", value)}
            value={attributes.postType}
            options={getPostTypes()}
          />
        </PanelBody>
        <PanelBody title={__("Taxonomy Options", "vc-blocks")}>
          <SelectControl
            label={__("Taxonomy?", "vc-blocks")}
            onChange={value => onSliderSettingToggle("taxonomy", value)}
            value={attributes.taxonomy}
            options={getTaxonomies()}
          />
        </PanelBody>
      </InspectorControls>
    );
  }
}

export default Inspector;
