import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import EditwithSelect from "./edit";

const attributes = {
  postType: {
    type: 'string',
    default: 'post'
  },
  taxonomy: {
    type: 'string',
    default: 'category'
  },
  currentCat: {
    type: 'string',
    default: 'all'
  }
}

registerBlockType("vc-blocks/post-filter", {
  title: __("Post Type Filter", "vc-blocks"),
  description: __(
    "Block to filter post types based on categories or taxonomies",
    "vc-blocks"
  ),
  attributes,
  category: "vc-category",
  keywords: [__("post", "vc-blocks"), __("filter", "vc-blocks")],
  icon: "admin-post",
  supports: {
    align: true
  },
  edit: EditwithSelect,
  save() {
    return null;
  }
});
