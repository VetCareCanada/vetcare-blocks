var default_hash = "all";

window.onload = function(event) {
  var grid = document.querySelector(".post-filter .grid");
  var selectedFilter = location.hash.substring(1) ? document.querySelector(
    '.post-filter .filter-tabs > .filter[data-slug="' +
      location.hash.substring(1) +
      '"]'
  ) :  document.querySelector(
    '.post-filter .filter-tabs > .filter[data-slug="all"]'
  );

  console.log(selectedFilter);

  function displayPosts() {
    if (location.hash == "" || !selectedFilter || selectedFilter === null) {
      location.hash = default_hash;
    }
    if (selectedFilter) {
      selectedFilter.classList.add("active");
    }

    for (let sibling of selectedFilter.parentNode.children) {
      if (sibling !== selectedFilter) sibling.classList.remove("active");
    }

    grid
      .querySelectorAll(":scope > .grid-item")
      .forEach(item => item.classList.remove("show"));
    grid
      .querySelectorAll("." + location.hash.substring(1))
      .forEach(item => item.classList.add("show"));
  }

  displayPosts();

  window.onhashchange = function() {
    displayPosts();
  };
};

document.addEventListener("DOMContentLoaded", function() {
  var filters = document.querySelectorAll(".post-filter .filter-tabs .filter");
  var mobileFilters = document.querySelectorAll(
    ".post-filter .filter-tabs-mobile .filter"
  );
  var postFilterMobileToggle = document.querySelector(
    ".post-filter .post-filter-mobile-toggle"
  );
  var mobileFilterTabsContent = document.querySelector(
    ".post-filter .filter-tabs-mobile .filter-tabs-content"
  );
  var currentFilter = document.querySelector(
    ".post-filter .post-filter-mobile-toggle .current-filter"
  );

  console.log(postFilterMobileToggle)

  postFilterMobileToggle.addEventListener("click", function(e) {
    this.classList.toggle("active");
    mobileFilterTabsContent.classList.toggle("hide");
  });
  function currentFilterInit() {
    for (let mobileFilter of mobileFilters) {
      if (
        mobileFilter.getAttribute("data-slug") === location.hash.substring(1)
      ) {
        return mobileFilter.textContent;
      } else {
        return default_hash;
      }
    }
  }
  currentFilter.innerHTML = currentFilterInit();
  // Filter
  for (let filter of filters) {
    filter.addEventListener("click", function() {
      location.hash = this.getAttribute("data-slug");
    });
  }
  for (let mobileFilter of mobileFilters) {
    mobileFilter.addEventListener("click", function() {
      location.hash = this.getAttribute("data-slug");
      currentFilter.innerHTML = mobileFilter.textContent
        ? mobileFilter.textContent
        : default_hash;
    });
  }
});
