<?php

	
	if ( $post_query->have_posts() ) :
		
		$post_filter_content .= '<div class="grid py-3 row">';
		while ( $post_query->have_posts() ) :
			$post_query->the_post();
			$selectedTerm = $attributes['taxonomy'] ? $attributes['taxonomy'] : 'category';
			$attachment_id = get_post_thumbnail_id($post_query->ID);
			$image = wp_get_attachment_image($attachment_id, 'full');
			$url          = get_permalink();
			$terms        = get_the_terms( $post_query->ID, $selectedTerm );
			$categories   = array();
			if ( $terms && is_array( $terms ) ) {
				foreach ( $terms as $key => $value ) {
					array_push( $categories, 'cat_' . $value->term_id );
				}
			}
			$categories = join( ' ', $categories );
			$post_filter_content .= '<a href="';
			$post_filter_content .= get_permalink();
			$post_filter_content .= '" ';
			$post_filter_content .='class="grid-item col-md-6 col-lg-4 ';
			$post_filter_content .= $categories;
			$post_filter_content .= ' all">';
			$post_filter_content .= get_the_post_thumbnail();
			$post_filter_content .= '</a>';
	endwhile; 
	$post_filter_content .= '</div>'; 
	wp_reset_postdata();	
	endif; // End Loop
	
	
