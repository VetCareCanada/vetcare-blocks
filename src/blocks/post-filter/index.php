<?php
/**
 * Serverside Post Filter render
 */

?>
<?php

/**
 * Render Callback function for Post Filter Block
 *
 * @param array $attributes Gutenberg Block attributes registered in register_block() in main plugin.php.
 *
 * @return string
 */
function vc_blocks_render_post_filter( $attributes ) {
    //print_r($attributes);
    // $attributes;
    $post_filter_content = '';
    $terms = get_terms(
        array(
            'taxonomy'   => $attributes['taxonomy'] ? $attributes['taxonomy'] : 'category',
            'hide_empty' => false,
        )
    );
    
    $tab_alignment        = $attributes['tabAlignment'] ? $attributes['tabAlignment'] : 'tab-left';
    $post_filter_content  = '<div class="post-filter">';
    
    include plugin_dir_path( __FILE__ ) . 'layouts/tabs.php';

    $args = array(
		'showposts' => -1,
		'post_type' => $attributes['postType'] ? $attributes['postType'] : 'post',
	);

	$post_query = new WP_Query( $args );

    include plugin_dir_path( __FILE__ ) . 'layouts/content.php';
    $post_filter_content .= '</div>'; // end of post-filter class (top element of component)
    return $post_filter_content;
}