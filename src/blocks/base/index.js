/**
 * Wordpress Dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

/**
 * Internal Dependencies
 */
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";

registerBlockType("vc-blocks/base", {
  title: __("Base", "vc-blocks"),
  description: __("Base", "vc-blocks"),
  category: "vc-category",
  keywords: [__("Base", "vc-blocks")],
  icon: "wordpress",
  edit,
  save
});
