/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Local dependencies
 */
import {Component} from "@wordpress/element";

class Edit extends Component {
    render() {
        const {className} = this.props;
        //console.log(this.props);
        const classes = classnames({
            "vc-base": true,
            [className]: !!className
        });
        return <div className={classes}>dummy text...</div>;
    }
}

export default Edit;
