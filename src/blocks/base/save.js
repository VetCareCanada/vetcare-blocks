/**
 * External dependencies
 */
import classnames from "classnames";

const classes = classnames({
  "vc-base": true
});

function save() {
  return <div className={classes}>dummy text...</div>;
}

export default save;
