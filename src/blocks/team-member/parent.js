import { registerBlockType, createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { InnerBlocks, InspectorControls } from "@wordpress/block-editor";
import { PanelBody, RangeControl } from "@wordpress/components";

const attributes = {
  columns: {
    type: "number",
    default: 2
  }
};

registerBlockType("vc-blocks/team-members", {
  title: __("Team Members", "vc-blocks"),
  description: __("Block showing a Team Members", "vc-blocks"),
  icon: "grid-view",
  category: "vc-category",
  supports: {
    html: false,
    align: ["wide", "full"]
  },

  transforms: {
    from: [
      {
        type: "block",
        blocks: ["core/gallery"],
        transform: ({ images }) => {
          let inner = images.map(({ alt, id, url }) => {
            return createBlock("vc-blocks/team-member", { alt, id, url });
          });
          return createBlock(
            "vc-blocks/team-members",
            {
              columns: images.length
            },
            inner
          );
        }
      },
      {
        type: "block",
        blocks: ["core/image"],
        isMultiBlock: true,
        transform: attributes => {
          let inner = attributes.map(({ alt, id, url }) => {
            return createBlock("vc-blocks/team-member", { alt, id, url });
          });
          return createBlock(
            "vc-blocks/team-members",
            {
              columns: attributes.length
            },
            inner
          );
        }
      }
    ]
  },
  keywords: [
    __("team", "vc-blocks"),
    __("member", "vc-blocks"),
    __("person", "vc-blocks")
  ],

  attributes,

  edit({ className, attributes, setAttributes }) {
    const { columns } = attributes;
    return (
      <div className={`${className} has-${columns}-columns`}>
        <InspectorControls>
          <PanelBody>
            <RangeControl
              label={__("Columns", "vc-blocks")}
              value={columns}
              onChange={columns => setAttributes({ columns })}
              min={1}
              max={6}
            />
          </PanelBody>
        </InspectorControls>
        <InnerBlocks
          allowedBlocks={["vc-blocks/team-member"]}
          template={[["vc-blocks/team-member"], ["vc-blocks/team-member"]]}
        />
      </div>
    );
  },
  save({ attributes }) {
    const { columns } = attributes;
    return (
      <div className={`has-${columns}-columns`}>
        <InnerBlocks.Content />
      </div>
    );
  }
});
