import "./style.editor.scss";
import "./parent";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import { RichText } from "@wordpress/block-editor";
import { Dashicon } from "@wordpress/components";

const attributes = {
  title: {
    type: "string",
    source: "html",
    selector: "h2"
  },
  subTitle: {
    type: "string",
    source: "html",
    selector: "h4"
  },
  info: {
    type: "string",
    source: "html",
    selector: "p"
  },
  id: {
    type: "number"
  },
  alt: {
    type: "string",
    source: "attribute",
    selector: "img",
    attribute: "alt"
  },
  url: {
    type: "string",
    source: "attribute",
    selector: "img",
    attribute: "src"
  },
  social: {
    type: "array",
    default: [],
    source: "query",
    selector: ".wp-block-vc-blocks-team-member__social ul li",
    query: {
      icon: {
        source: "attribute",
        attribute: "data-icon"
      },
      link: {
        source: "attribute",
        selector: "a",
        attribute: "href"
      }
    }
  }
};

registerBlockType("vc-blocks/team-member", {
  title: __("Team Member", "vc-blocks"),
  description: __("Block showing a Team Member", "vc-blocks"),
  icon: "admin-users",
  parent: ["vc-blocks/team-members"],
  supports: {
    reusable: false,
    html: false
  },
  category: "vc-category",
  keywords: [
    __("team", "vc-blocks"),
    __("member", "vc-blocks"),
    __("person", "vc-blocks")
  ],
  attributes,
  save: ({ attributes }) => {
    //console.log(attributes);
    const { title, subTitle, info, url, id, alt, social } = attributes;
    return (
      <div>
        {url && (
          <img src={url} alt={alt} className={id ? `wp-image-${id}` : null} />
        )}
        {title && (
          <RichText.Content
            tagName="h2"
            value={title}
            className={"wp-block-vc-blocks-team-member__title"}
          />
        )}
        {subTitle && (
          <RichText.Content
            tagName="h4"
            value={subTitle}
            className={"wp-block-vc-blocks-team-member__sub-title"}
          />
        )}
        {info && (
          <RichText.Content
            tagName="p"
            value={info}
            className={"wp-block-vc-blocks-team-member__info"}
          />
        )}

        {social.length > 0 && (
          <div className={"wp-block-vc-blocks-team-member__social"}>
            <ul>
              {social.map((item, index) => {
                return (
                  <li key={index} data-icon={item.icon}>
                    <a
                      href={item.link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Dashicon icon={item.icon} size={20} />
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    );
  },
  edit
});
