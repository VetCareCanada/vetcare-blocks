import { Component } from "@wordpress/element";
import {
  BlockControls,
  MediaPlaceholder,
  RichText,
  MediaUploadCheck,
  MediaUpload,
  URLInput,
  InspectorControls
} from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { isBlobURL } from "@wordpress/blob";
import {
  Spinner,
  Toolbar,
  withNotices,
  IconButton,
  PanelBody,
  TextareaControl,
  TextControl,
  SelectControl,
  Dashicon,
  Tooltip
} from "@wordpress/components";

import { withSelect } from "@wordpress/data";

import {
  SortableContainer,
  SortableElement,
  arrayMove
} from "react-sortable-hoc";

class TeamMemberEdit extends Component {
  state = {
    selectedLink: null
  };

  componentDidMount() {
    const { attributes, setAttributes } = this.props;
    const { url, id } = attributes;
    if (url && isBlobURL(url) && !id) {
      setAttributes({
        url: "",
        alt: ""
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isSelected && !this.props.isSelected) {
      this.setState({ selectedLink: null });
    }
  }

  onChangeTitle = title => {
    this.props.setAttributes({ title: title });
  };
  onChangeSubTitle = subTitle => {
    this.props.setAttributes({ subTitle: subTitle });
  };
  onChangeInfo = info => {
    this.props.setAttributes({ info: info });
  };

  onSelectImage = image => {
    const { id, alt, url } = image;
    this.props.setAttributes({ id, alt, url });
  };
  onSelectURL = url => {
    this.props.setAttributes({
      url,
      id: null,
      alt: ""
    });
  };

  onChangeAlt = alt => {
    this.props.setAttributes({ alt });
  };

  onUploadError = message => {
    const { noticeOperations } = this.props;
    noticeOperations.createErrorNotice(message);
  };
  removeImage = () => {
    const { setAttributes } = this.props;
    setAttributes({
      url: "",
      alt: "",
      id: null
    });
  };

  getImageSizes() {
    const { image, imageSizes } = this.props;
    if (!image) return [];
    let options = [];
    const sizes = image.media_details.sizes;
    for (const key in sizes) {
      const size = sizes[key];
      const imageSize = imageSizes.find(size => size.slug === key);
      if (imageSize) {
        options.push({
          label: imageSize.name,
          value: size.source_url
        });
      }
    }
    return options;
  }

  onImageSizeChange = url => {
    this.props.setAttributes({ url });
  };

  addNewLink = () => {
    const { attributes, setAttributes } = this.props;
    const { social } = attributes;
    setAttributes({
      social: [...social, { icon: "wordpress", link: "#" }]
    });
    this.setState({ selectedLink: social.length });
  };

  updateSocialItem = (type, value) => {
    const { attributes, setAttributes } = this.props;
    const { social } = attributes;
    const { selectedLink } = this.state;
    let new_social = [...social];
    new_social[selectedLink][type] = value;

    setAttributes({ social: new_social });
  };

  removeLink = e => {
    e.preventDefault();
    const { attributes, setAttributes } = this.props;
    const { social } = attributes;
    const { selectedLink } = this.state;

    setAttributes({
      social: [
        ...social.slice(0, selectedLink),
        ...social.slice(selectedLink + 1)
      ]
    });
    this.setState({ selectedLink: null });
  };

  onSortEnd = (oldIndex, newIndex) => {
    const { attributes, setAttributes } = this.props;
    const { social } = attributes;
    let new_social = arrayMove(social, oldIndex, newIndex);

    setAttributes({ social: new_social });
    this.setState({ selectedLink: null });
  };

  render() {
    //console.log(this.props);
    const { className, attributes, noticeUI, isSelected } = this.props;
    const { title, subTitle, info, url, alt, id, social } = attributes;
    let SortableList = SortableContainer(() => {
      return (
        <ul>
          {social.map((item, index) => {
            let SortableItem = SortableElement(() => {
              return (
                <li
                  key={index}
                  onClick={() => this.setState({ selectedLink: index })}
                  className={this.state.selectedLink === index && "is-selected"}
                >
                  <Dashicon icon={item.icon} size={16} />
                </li>
              );
            });
            return <SortableItem key={index} index={index} />;
          })}

          {isSelected && (
            <li className={"wp-block-vc-blocks-team-member__addIconLi"}>
              <Tooltip text={__("Add Item", "vc-blocks")}>
                <button
                  onClick={this.addNewLink}
                  className={"wp-block-vc-blocks-team-member__addIcon"}
                >
                  <Dashicon icon="plus" size={14} />
                </button>
              </Tooltip>
            </li>
          )}
        </ul>
      );
    });
    return (
      <>
        <InspectorControls>
          <PanelBody title={__("Image Settings", "vc-blocks")}>
            {url && !isBlobURL(url) && (
              <TextareaControl
                label={__("ALt Text", "vc-blocks")}
                value={alt}
                onChange={this.onChangeAlt}
                help={__("Alternative text", "vc-blocks")}
              />
            )}
            <SelectControl
              label={__("Image Size", "vc-blocks")}
              options={this.getImageSizes()}
              onChange={this.onImageSizeChange}
            />
          </PanelBody>
        </InspectorControls>
        <BlockControls>
          {url && (
            <Toolbar>
              {id && (
                <MediaUploadCheck>
                  <MediaUpload
                    onSelect={this.onSelectImage}
                    allowedTypes={["image"]}
                    value={id}
                    render={({ open }) => {
                      return (
                        <IconButton
                          className="components-icon-button components-toolbar__control"
                          label={__("Edit Image", "vc-blocks")}
                          onClick={open}
                          icon="edit"
                        />
                      );
                    }}
                  />
                </MediaUploadCheck>
              )}
              <IconButton
                className="components-icon-button components-toolbar__control"
                label={__("Remove Image", "vc-blocks")}
                onClick={this.removeImage}
                icon="trash"
              />
            </Toolbar>
          )}
        </BlockControls>
        <div className={className}>
          {url ? (
            <>
              <img src={url} alt={alt} />
              {isBlobURL(url) && <Spinner />}
            </>
          ) : (
            <MediaPlaceholder
              icon="format-image"
              onSelect={this.onSelectImage}
              onSelectURL={this.onSelectURL}
              onError={this.onUploadError}
              //accept="image/*"
              allowedTypes={["image"]}
              labels={{
                title: "Team Member Image",
                instructions: "upload member portrait"
              }}
              notices={noticeUI}
            />
          )}

          <RichText
            tagName="h2"
            className={"wp-block-vc-blocks-team-member__title"}
            onChange={this.onChangeTitle}
            value={title}
            placeholder={__("Member Name", "vc-blocks")}
            formattingControls={[]}
          />
          <RichText
            tagName="h4"
            className={"wp-block-vc-blocks-team-member__sub-title"}
            onChange={this.onChangeSubTitle}
            value={subTitle}
            placeholder={__("Member Designation", "vc-blocks")}
            formattingControls={[]}
          />
          <RichText
            tagName="p"
            className={"wp-block-vc-blocks-team-member__info"}
            onChange={this.onChangeInfo}
            value={info}
            placeholder={__("Member Info", "vc-blocks")}
          />
          <div className={"wp-block-vc-blocks-team-member__social"}>
            <SortableList
              axis="x"
              lockAxis="x"
              helperClass={"social-dragging"}
              distance={10}
              onSortEnd={({ oldIndex, newIndex }) =>
                this.onSortEnd(oldIndex, newIndex)
              }
            />
            {/* <ul>
              {social.map((item, index) => {
                return (
                  <li
                    key={index}
                    onClick={() => this.setState({ selectedLink: index })}
                    className={
                      this.state.selectedLink === index && "is-selected"
                    }
                  >
                    <Dashicon icon={item.icon} size={16} />
                  </li>
                );
              })}
              {isSelected && (
                <li className={"wp-block-vc-blocks-team-member__addIconLi"}>
                  <Tooltip text={__("Add Item", "vc-blocks")}>
                    <button
                      onClick={this.addNewLink}
                      className={"wp-block-vc-blocks-team-member__addIcon"}
                    >
                      <Dashicon icon="plus" size={14} />
                    </button>
                  </Tooltip>
                </li>
              )}
            </ul>*/}
          </div>
          {this.state.selectedLink !== null && (
            <div className={"wp-block-vc-blocks-team-member__linkForm"}>
              <TextControl
                label={__("Icon", "vc-blocks")}
                value={social[this.state.selectedLink].icon}
                onChange={icon => this.updateSocialItem("icon", icon)}
              />
              <URLInput
                label={__("URL", "vc-blocks")}
                value={social[this.state.selectedLink].link}
                onChange={url => this.updateSocialItem("link", url)}
              />
              <a
                className={"wp-block-vc-blocks-team-member__removeLink"}
                onClick={this.removeLink}
              >
                {__("Remove Link", "vc-blocks")}
              </a>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default withSelect((select, props) => {
  const id = props.attributes.id;
  return {
    image: id ? select("core").getMedia(id) : null,
    imageSizes: select("core/editor").getEditorSettings().imageSizes
  };
})(withNotices(TeamMemberEdit));
