import {__} from "@wordpress/i18n";
import {
    RichTextToolbarButton,
    RichTextShortcut
} from "@wordpress/block-editor";
import {Fragment} from "@wordpress/element";

const {toggleFormat} = wp.richText;
const {registerFormatType} = wp.richText;

/**
 * adds underline format to RichText block
 */
export const underline = {
    name: "vc-blocks/underline",
    title: __("Underline"),
    tagName: "u",
    className: null,
    attributes: {
        style: "style"
    },
    edit({isActive, value, onChange}) {
        const onToggle = () => {
            onChange(
                toggleFormat(value, {
                    type: "vc-blocks/underline",
                    attributes: {
                        //style: 'text-decoration: underline;',
                    }
                })
            );
        };
        return (
            <Fragment>
                <RichTextShortcut type="primary" character="u" onUse={onToggle}/>
                <RichTextToolbarButton
                    icon="editor-underline"
                    title={__("Underline")}
                    onClick={onToggle}
                    isActive={isActive}
                    shortcutType="primary"
                    shortcutCharacter="u"
                />
            </Fragment>
        );
    }
};

/**
 * adds Open in Video Modal format to RichText block
 */
export const videoModal = {
    name: "vc-blocks/video-modal",
    title: __("Open in Video Modal"),
    tagName: "span",
    className: "video-link",
    attributes: {
        style: "style"
    },
    edit({isActive, value, onChange}) {
        const onToggle = () => {
            onChange(
                toggleFormat(value, {
                    type: "vc/video-modal",
                    attributes: {}
                })
            );
        };
        return (
            <RichTextToolbarButton
                icon="editor-underline"
                title={__("Open In Video Modal")}
                onClick={onToggle}
                isActive={isActive}
            />
        );
    }
};

function registerFormats() {
    [underline, videoModal].forEach(({name, ...settings}) =>
        registerFormatType(name, settings)
    );
}

registerFormats();
