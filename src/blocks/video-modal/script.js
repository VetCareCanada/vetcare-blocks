import "./style.scss";
import $ from "jquery";

(function ($) {
    $(document).ready(function () {
        let $videoSrc;
        $(".video-link a, .video-link .swiper-slide__img i").on("click", function (e) {
            $videoSrc = $(this).attr("href") || $(this).prev('img').attr("data-videourl");
            const videoUrlObj = parseVideo($videoSrc);
            if (videoUrlObj.type !== "youtube" && videoUrlObj.type !== "vimeo") {
                return true;
            }
            $videoSrc = createEmbedUrl($videoSrc);
            //console.log($videoSrc);
            e.preventDefault();
            //check is modal is already added to the site
            if ($("#video-modal").length <= 0) {
                let modalHtml = `
          <div id="video-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <!-- 16:9 aspect ratio -->
                        <div class="ratio ratio-16x9">
                            <iframe class="embed-responsive-item" src="" id="video-modal-iframe" allowscriptaccess="always" allow="autoplay"></iframe>
                        </div>

                    </div>
                </div>
            </div>
        </div>
      `;
                $("body").append(modalHtml);

                // when the modal is opened autoplay it
                $("#video-modal").on("show.bs.modal", function () {
                    $("#video-modal-iframe").attr(
                        "src",
                        $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0"
                    );
                });

                // stop playing the youtube video when I close the modal
                $("#video-modal").on("hide.bs.modal", function () {
                    $("#video-modal-iframe").attr("src", "");
                });
            }

            //show the modal
            $("#video-modal").modal("show");
        });

        function parseVideo(url) {
            // - Supported YouTube URL formats:
            //   - http://www.youtube.com/watch?v=My2FRPA3Gf8
            //   - http://youtu.be/My2FRPA3Gf8
            //   - https://youtube.googleapis.com/v/My2FRPA3Gf8
            // - Supported Vimeo URL formats:
            //   - http://vimeo.com/25451551
            //   - http://player.vimeo.com/video/25451551
            // - Also supports relative URLs:
            //   - //player.vimeo.com/video/25451551

            url.match(
                /(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/
            );
            var type = "";
            if (RegExp.$3.indexOf("youtu") > -1) {
                type = "youtube";
            } else if (RegExp.$3.indexOf("vimeo") > -1) {
                type = "vimeo";
            }

            return {
                type: type,
                id: RegExp.$6
            };
        }

        function createEmbedUrl(url) {
            const videoObj = parseVideo(url);
            let embedUrl = "";
            if (videoObj.type === "youtube") {
                embedUrl = "//www.youtube.com/embed/" + videoObj.id;
            } else if (videoObj.type === "vimeo") {
                embedUrl = "//player.vimeo.com/video/" + videoObj.id;
            }
            return embedUrl;
        }
    });
})($);
