import { InnerBlocks } from "@wordpress/block-editor";

function save() {
  return (
    <div>
      <InnerBlocks.Content />
    </div>
  );
}

export default save;
