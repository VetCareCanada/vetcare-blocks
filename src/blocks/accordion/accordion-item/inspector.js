/**
 * Wordpress dependencies
 */
import { Component } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, ToggleControl } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

class Inspector extends Component {
  render() {
    const { attributes } = this.props;
    const { expanded } = attributes;

    return (
      <InspectorControls>
        <PanelBody title={__("Accordion Item settings", "vc-blocks")}>
          <ToggleControl
            label={__("Expanded?", "vc-blocks")}
            checked={expanded}
            onChange={value => this.props.setAttributes({ expanded: value })}
          />
        </PanelBody>
      </InspectorControls>
    );
  }
}

export default Inspector;
