/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {Component} from '@wordpress/element';
import {InnerBlocks, RichText} from "@wordpress/block-editor";
import slugify from "slugify";

/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Local Dependencies
 */
import Inspector from "./inspector";

/**
 * Constants
 */
const TEMPLATE = [
  ['core/paragraph', {placeholder: __('Add content…', 'vc-blocks')}],
];

class Edit extends Component {

  render() {
    const {attributes, setAttributes, isSelected} = this.props;
    const {title, expanded} = attributes;
    const accordionSlug = slugify(title, {strict: true});
    const accordionHeaderClasses = classnames({
      "accordion-header": true,
      "collapsed": !expanded
    });
    const accordionContentClasses = classnames({
      "accordion-collapse": true,
      "collapse": true,
      "show": expanded
    });
    const handleClick = event => {
      event.target.parentElement?.nextElementSibling.classList.toggle("show");

    }
    return (
      <div>
        {isSelected && <Inspector {...this.props} />}
        <div className={accordionHeaderClasses}
             data-bs-toggle="collapse"
             data-bs-target={`#collapse-${accordionSlug}`} aria-expanded={expanded}
             aria-controls={`collapse-${accordionSlug}`}
             onClick={handleClick} >
          <RichText
            tagName="h5"
            value={title}
            onChange={title => {
              setAttributes({title})
            }}
          />
        </div>

        <div id={`collapse-${accordionSlug}`} className={accordionContentClasses}>
          <div className="accordion-body">
            <InnerBlocks
              template={TEMPLATE}
              templateInsertUpdatesSelection={false}
            />
          </div>
        </div>
      </div>
    );
  }
}


export default Edit;
