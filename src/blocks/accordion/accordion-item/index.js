import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
  title: {
    type: "string",
    default: "title here...",
    source: "html",
    selector: ".accordion-header h5"
  },
  expanded: {
    type: "boolean",
    default: false
  }
};

registerBlockType("vc-blocks/accordion-item", {
  title: __("Accordion Item", "vc-blocks"),
  description: __("Accordion/Collapse", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Accordion", "vc-blocks"),
    __("Collapse", "vc-blocks"),
    __("Faqs", "vc-blocks")
  ],
  //parent: ['vc-blocks/accordion'],
  supports: {
    reusable: false,
    html: false,
    //inserter: false,
  },
  icon: "list-view",
  attributes,
  edit,
  save
});
