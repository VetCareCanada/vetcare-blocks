import {InnerBlocks, RichText} from "@wordpress/block-editor";
import slugify from "slugify";
import classnames from "classnames";

function save(props) {
  const {attributes} = props;
  const {title, expanded} = attributes;
  const accordionSlug = slugify(title, {strict: true});
  const accordionHeaderClasses = classnames({
    "accordion-header": true,
    "collapsed": !expanded
  });
  const accordionContentClasses = classnames({
    "accordion-collapse": true,
    "collapse": true,
    "show": expanded
  });
  return (
    <div>
      <div className={accordionHeaderClasses}
           data-bs-toggle="collapse"
           data-bs-target={`#collapse-${accordionSlug}`} aria-expanded="true"
           aria-controls={`collapse-${accordionSlug}`}>
        <RichText.Content
          tagName="h5"
          value={title}
        />
      </div>
      <div id={`collapse-${accordionSlug}`} className={accordionContentClasses}>
        <div className="accordion-body">
          <InnerBlocks.Content/>
        </div>
      </div>
    </div>
  );
}


export default save;
