import "./style.editor.scss";
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
  count: {
    type: "number",
    default: 1
  }
};

registerBlockType("vc-blocks/accordion", {
  title: __("Accordion", "vc-blocks"),
  description: __("Accordion/Collapse", "vc-blocks"),
  category: "vc-category",
  keywords: [
    __("Accordion", "vc-blocks"),
    __("Collapse", "vc-blocks"),
    __("Faqs", "vc-blocks")
  ],
  icon: "list-view",
  attributes,
  edit,
  save
});
