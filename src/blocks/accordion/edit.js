import {Component} from "@wordpress/element";
import classnames from "classnames";
import {InnerBlocks} from "@wordpress/block-editor";

const ALLOWED_BLOCKS = ['vc-blocks/accordion-item'];

class Edit extends Component {

  render() {
    const {
      className,
    } = this.props;
    const classes = classnames({
      accordions: true,
      [className]: !!className
    });

    return (
      <div className={classes}>
        <InnerBlocks
          allowedBlocks={ALLOWED_BLOCKS}
        />
      </div>
    );
  }
}

export default Edit;
