import {Component} from "@wordpress/element";
import classnames from "classnames";
import {
    BlockControls,
    InnerBlocks,
    MediaPlaceholder,
    InspectorControls,
    MediaUpload,
    MediaUploadCheck,
    PanelColorSettings,
    withColors,
    URLInputButton
} from "@wordpress/block-editor";
import {
    Button,
    Toolbar,
    RangeControl,
    SelectControl,
    PanelBody,
    TextControl
} from "@wordpress/components";
import {__} from "@wordpress/i18n";
import {compose} from "@wordpress/compose";
import {withSelect} from "@wordpress/data";

import DivOrAnchorWrapper from "./div-or-anchor-wrapper";

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/list",
  "core/button",
  "core/group",
  "core/textColumns",
  "core/html"
];
const BLOCK_TEMPLATE = [
  ["core/heading", {content: "Enter Card title...", level: 3}],
  [
    "core/paragraph",
    {
        content:
        "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer..."
    }
    ]
    ];


    class Edit extends Component {
        componentDidUpdate()
        {
            this.props.setAttributes({
                activateOverlay: this.props.contentOverlay
            });
        }

        onSelectImage = image => {
            const {id, url, alt} = image;
            this.props.setAttributes({
                imageURL: url,
                imageID: id,
                imageAlt: alt
            });
        };

        onChangeAnimation = cardAnimation => {
        //console.log(animation);
            this.props.setAttributes({
                cardAnimation
            });
        };

        removeImage = () => {
            const {setAttributes} = this.props;
            setAttributes({
                imageURL: "",
                imageID: null,
                imageAlt: ""
            });
        };


        render()
        {
            const {
                attributes,
                className,
                setAttributes,
                overlayColor,
                setOverlayColor,
                addImage
            } = this.props;
            const {
                dimRatio,
                imageURL,
                imageAlt,
                imageID,
                cardAnimation,
                activateOverlay,
                iconClass,
                href
            } = attributes;
            const classes = classnames({
                col: true,
                [className]: !!className
            });

            const cardClasses = classnames({
                card: true,

                activateOverlay: activateOverlay ? true : false
            });
            const cardWrapperClasses = classnames({
                "card-content": true,
                    [cardAnimation === undefined ? "" : cardAnimation]: true
            });

            const overlayClasses = classnames({
                    [overlayColor.color === undefined
                ? "has-white-background-color"
                : overlayColor.class]: true,
                backgroundOverlay: true
            });
            const contentStyles = {};
        // contentStyles.backgroundImage = backgroundImageUrl ? `url(${backgroundImageUrl})` : null;
            contentStyles["--overlayDimRatio"] = (dimRatio / 100).toString();

            return (
            <>
            <InspectorControls>
              <PanelColorSettings
              title={__("Overlay Color", "vc-blocks")}
              initialOpen={true}
              colorSettings={[
                    {
                        value: overlayColor.color,
                        onChange: (...args) => {
                            setAttributes({
                                customGradient: undefined
                            });
                          setOverlayColor(...args);
                        },
                        label: __("Overlay Color", "vc-blocks")
                    }
                    ]}
            >
            <RangeControl
              label={__("Background Opacity", "vc-blocks")}
              value={dimRatio}
              onChange={newDimRation =>
                    setAttributes({dimRatio: newDimRation})
                  }
              min={0}
              max={100}
              required
            />
            </PanelColorSettings>
            <PanelBody title={__("Card Animations", "vc-blocks")}>
            <SelectControl
              label={__("Card Animation", "vc-blocks")}
              value={cardAnimation}
              options={[
                    {label: "None", value: ""},
                    {label: "Slide In Top", value: "slide-in-text-top"},
                    {label: "Slide In Bottom", value: "slide-in-text-bottom"},
                    {label: "Slide In Left", value: "slide-in-text-left"},
                    {label: "Slide In Right", value: "slide-in-text-right"}
                    ]}
              onChange={this.onChangeAnimation}
            />
            </PanelBody>
            <PanelBody title={__("Icon", "vc-blocks")}>
            <TextControl
              label="Icon Class or other classes"
              value={iconClass}
              onChange={iconClass => {
                    setAttributes({iconClass});
                    }}
            />
            </PanelBody>
            </InspectorControls>
            <BlockControls>
            <Toolbar>
            <URLInputButton
              url={href}
              onChange={(url, post) => setAttributes({href: url, text: (post && post.title) || 'Click here'})}
            />
            </Toolbar>
            {imageURL && (
                <Toolbar>
                {imageID && (
                    <MediaUploadCheck>
                    <MediaUpload
                    onSelect={this.onSelectImage}
                    allowedTypes={["image"]}
                    value={imageID}
                    render={({open}) => {
                        return (
                        <Button
                          className="components-icon-button components-toolbar__control"
                          label={__("Edit Image", "vc-blocks")}
                          onClick={open}
                          icon="edit"
                        />
                      );
                        }}
                  />
                </MediaUploadCheck>
                )}
              <Button
                className="components-icon-button components-toolbar__control"
                label={__("Remove Image", "vc-blocks")}
                onClick={this.removeImage}
                icon="trash"
              />
            </Toolbar>
            )}
            </BlockControls>
            <div className={classes}>
            <DivOrAnchorWrapper href={href} className={cardClasses}>
            {addImage && !imageURL && (
                <MediaPlaceholder
                icon="format-image"
                allowedTypes={["image"]}
                onSelect={this.onSelectImage}
                labels={{
                    title: "card image",
                    instructions: "upload card image"
                    }}
              />
            )}
            {imageURL && addImage && <img src={imageURL} alt={imageAlt} className={imageID ? `wp-image-${imageID}` : null}/>}
            <div className={cardWrapperClasses}>
              {overlayColor && (
                    <div style={contentStyles} className={overlayClasses}></div>
              )}
              {iconClass && <div className={iconClass}></div>}
              <div className="card-body">
                <InnerBlocks
                  allowedBlocks={ALLOWED_BLOCKS}
                  template={BLOCK_TEMPLATE}
                />
              </div>
            </div>
            </DivOrAnchorWrapper>
            {/* <div className={cardClasses} href={href}>

            </div>*/}
            </div>
            </>
            );
        }
    }

    export default compose(
        withColors({overlayColor: "background-color"}),
        withSelect((select, props) => {
            const childClientId = props.clientId;
            const parentClientId = select("core/block-editor").getBlockRootClientId(
                childClientId
            );
        const imageID = props.attributes.id;
        const parentContentOverlay = select(
            "core/block-editor"
        ).getBlocksByClientId(parentClientId)[0].attributes.contentOverlay;

        return {
            addImage: select("core/block-editor").getBlocksByClientId(
                parentClientId
            )[0].attributes.addImage,
        contentOverlay: parentContentOverlay,
        image: imageID ? select("core").getMedia(imageID) : null
            };
        })
    )(Edit);
