import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import {
    BlockControls,
    MediaUploadCheck,
    InnerBlocks,
    InspectorControls,
    MediaUpload,
    MediaPlaceholder
} from "@wordpress/block-editor";
import {
    PanelBody,
    RangeControl,
    ToggleControl,
    Button,
    Toolbar
} from "@wordpress/components";
import classnames from "classnames";

const attributes = {
    addBackground: {
        type: "boolean",
        default: "false"
    },
    backgroundImage: {
        type: "string",
        default: null
    },
    backgroundImageID: {
        type: "number"
    },
    backgroundColor: {
        type: "string",
        default: null
    },
    columns: {
        type: "number",
        default: 2
    },
    addImage: {
        type: "boolean",
        default: false
    },
    contentOverlay: {
        type: "boolean",
        default: false
    },
    centerAlignCards: {
        type: "boolean",
        default: false
    }
};

registerBlockType("vc-blocks/cards", {
    title: __("Cards", "vc-blocks"),
    description: __("Block showing Cards", "vc-blocks"),
    icon: "grid-view",
    category: "vc-category",
    supports: {
        html: false,
        align: ["full"]
    },
    keywords: [__("Cards", "vc-blocks")],

    attributes,

    edit({className, attributes, setAttributes, clientId}) {
        const {
            columns,
            addImage,
            contentOverlay,
            addBackground,
            backgroundImage,
            backgroundImageID,
            centerAlignCards
        } = attributes;
        //console.log(backgroundImage);
        const classes = classnames({
            "vc-cards": true,
            "vc-cards--center": centerAlignCards,
            [className]: !!className
        });

        const classesInner = classnames({
            /*"row": true,*/
            [`has-${columns}-columns`]: true,
            "has-bg-image": backgroundImage && addBackground ? true : false
        });

        const contentStyle =
            addBackground && backgroundImage
                ? {
                    backgroundImage: "url(" + backgroundImage + ")",
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "center",
                    backgroundSize: "cover"
                }
                : null;

        const selectBackgroundImage = image => {
            const {url, id} = image;
            setAttributes({
                backgroundImage: url,
                backgroundImageID: id
            });
        };

        const removeImage = () => {
            setAttributes({
                backgroundImage: "",
                backgroundImageID: null
            });
        };

        return (
            <div style={contentStyle} className={classes}>
                <InspectorControls>
                    <PanelBody>
                        <RangeControl
                            label={__("Columns", "vc-blocks")}
                            value={columns}
                            onChange={columns => setAttributes({columns})}
                            min={1}
                            max={6}
                        />
                        <ToggleControl
                            label={__("Add a Background Image?", "vc-blocks")}
                            onChange={() => setAttributes({addBackground: !addBackground})}
                            checked={addBackground}
                        />
                        <ToggleControl
                            label={__("Add Image to Cards?", "vc-blocks")}
                            onChange={() => setAttributes({addImage: !addImage})}
                            checked={addImage}
                        />
                        <ToggleControl
                            label={__("Center align Cards?", "vc-blocks")}
                            onChange={() => setAttributes({centerAlignCards: !centerAlignCards})}
                            checked={centerAlignCards}
                        />

                        {addImage && (
                            <ToggleControl
                                label={__("Make Content overlay?", "vc-blocks")}
                                onChange={() =>
                                    setAttributes({
                                        contentOverlay: !contentOverlay
                                    })
                                }
                                checked={contentOverlay}
                            />
                        )}
                    </PanelBody>
                </InspectorControls>
                <div className={classesInner}>
                    <BlockControls>
                        {backgroundImage && (
                            <Toolbar>
                                {backgroundImageID && (
                                    <MediaUploadCheck>
                                        <MediaUpload
                                            onSelect={image => {
                                                selectBackgroundImage(image);
                                            }}
                                            allowedTypes={["image"]}
                                            value={backgroundImageID}
                                            render={({open}) => {
                                                return (
                                                    <Button
                                                        className="components-icon-button components-toolbar__control"
                                                        label={__("Edit Image", "vc-blocks")}
                                                        onClick={open}
                                                        icon="edit"
                                                    />
                                                );
                                            }}
                                        />
                                    </MediaUploadCheck>
                                )}
                                <Button
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Remove Image", "vc-blocks")}
                                    onClick={() => removeImage()}
                                    icon="trash"
                                />
                            </Toolbar>
                        )}
                    </BlockControls>
                    {addBackground && !backgroundImage && (
                        <MediaPlaceholder
                            icon="format-image"
                            allowedTypes={["image"]}
                            onSelect={image => {
                                selectBackgroundImage(image);
                            }}
                            labels={{
                                title: "card image",
                                instructions: "upload card image"
                            }}
                        />
                    )}
                    <InnerBlocks
                        allowedBlocks={["vc-blocks/card"]}
                        template={[["vc-blocks/card"], ["vc-blocks/card"]]}
                        parentClientId={clientId}
                    />
                </div>
            </div>
        );
    },
    save(props) {
        const {attributes} = props;
        //console.log(props);
        const {columns, addBackground, backgroundImage, centerAlignCards} = attributes;
        const contentStyle =
            backgroundImage && addBackground
                ? {
                    backgroundImage: "url(" + backgroundImage + ")",
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "center",
                    backgroundSize: "cover"
                }
                : null;
        const classes = classnames({
            "vc-cards": true,
            "vc-cards--center": centerAlignCards,
        });
        const classesInner = classnames({
            row: true,

            "row-cols-1": true,
            "row-cols-md-2": true,
            [`row-cols-lg-${columns}`]: true,
            "has-bg-image": backgroundImage && addBackground ? true : false
        });
        return (
            <div style={contentStyle} className={classes}>
                <div className={classesInner}>
                    <InnerBlocks.Content/>
                </div>
            </div>
        );
    }
});
