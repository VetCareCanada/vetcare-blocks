import "./style.editor.scss";
import "./parent";
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
    href: {
        type: "string",
        selector: ".wp-block-vc-blocks-card > a",
        source: "attribute",
        attribute: "href"

    },
    overlayColor: {
        type: "string"
    },
    customOverlayColor: {
        type: "string"
    },
    dimRatio: {
        type: "number",
        default: 20
    },
    imageURL: {
        type: "string"
    },
    imageAlt: {
        type: "string",
        source: "attribute",
        selector: "img",
        attribute: "alt"
    },
    imageID: {
        type: "number"
    },
    cardAnimation: {
        type: "string"
    },
    activateOverlay: {
        type: "boolean"
    },
    iconClass: {
        string: "string"
    }
};

registerBlockType("vc-blocks/card", {
    title: __("Card", "vc-blocks"),
    description: __("Card", "vc-blocks"),
    category: "vc-category",
    keywords: [__("Card", "vc-blocks")],
    icon: "format-image",
    parent: ["vc-blocks/cards"],
    attributes,
    edit,
    save
});
