import "./style.scss";
import "./extensions/list-styles/script";
import "./hoc/spacing-controls/script";
//import "./blocks/base/script";
//import "./blocks/secondblock/script";
import "./blocks/team-member/script";
import "./blocks/latest-posts/script";
import "./blocks/cta/script";
import "./blocks/img-text/script";
import "./blocks/accordion/script";
import "./blocks/map-text/script";
import "./blocks/video-modal/script";
import "./blocks/multi-cta/script";
import "./blocks/cards/script";
//import "./blocks/slider/script";
import "./blocks/swiper-slider/script";
// import "./blocks/swiper/script";
import "./blocks/pricing-table/script";
import "./blocks/fancy-heading/script";
import "./blocks/post-filter/script";
import "./blocks/mailchimp/script";
import "./blocks/vctab/script";
import "./blocks/vctab-pane/script";


