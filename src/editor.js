import "./style.editor.scss";

/* Extensions */
import "./extensions/list-styles";

/* hoc */
import "./hoc/spacing-controls";
import "./hoc/lightbox";

/*All custom blocks should be imported below*/
//import "./blocks/base";
import "./blocks/team-member";
import "./blocks/cta";
import "./blocks/latest-posts";

import "./blocks/img-text";
import "./blocks/accordion";
import "./blocks/accordion/accordion-item";
import "./blocks/map-text";
import "./blocks/video-modal";
import "./blocks/multi-cta";
import "./blocks/cards";
import "./blocks/tabs";
import "./blocks/vctab";
import "./blocks/vctab-pane";
// import "./blocks/swiper";
import "./blocks/swiper-slider";
import "./blocks/swiper-slider/swiper-single-slide";
import "./blocks/pricing-table";
import "./blocks/pricing-table/pricing-table-item";
import "./blocks/post-filter";
import "./blocks/banner";
import "./blocks/page-header";
import "./blocks/mailchimp";
import "./blocks/fancy-heading";
import "./blocks/swiper-testimonials";
import "./blocks/video-banner";




