/**
 * WordPress Dependencies
 */
import {createHigherOrderComponent} from "@wordpress/compose";
import {addFilter} from "@wordpress/hooks";
import {PanelBody, ToggleControl} from "@wordpress/components";
import {InspectorControls} from '@wordpress/block-editor';
import {__} from "@wordpress/i18n";
import classnames from "classnames";

const blocksWithLightboxSupport = ['core/gallery'];

const withLightbox = createHigherOrderComponent((BlockEdit) => {
  return (props) => {
    const {name, attributes, setAttributes, isSelected} = props;
    const {lightbox, fancyLightbox} = attributes;

    const supportsLightbox = blocksWithLightboxSupport.includes(name);
    return (
      <>
        <BlockEdit {...props} />
        {isSelected && supportsLightbox && (!!props.attributes?.images?.length || !!props.attributes.id) && (
          <InspectorControls>
            <PanelBody title={__("Lightbox Settings", "vc-blocks")}>
              <ToggleControl
                label={__('Enable Lightbox', 'vc-blocks')}
                checked={!!lightbox}
                onChange={() => setAttributes({lightbox: !lightbox})}
              />
              {lightbox && (
                <ToggleControl
                  label={__('Enable fancy style', 'vc-blocks')}
                  checked={!!fancyLightbox}
                  onChange={() => setAttributes({fancyLightbox: !fancyLightbox})}
                />
              )}

            </PanelBody>
          </InspectorControls>
        )}
      </>
    );
  };
}, "withLightbox");

const addAttributes = (settings, name) => {
  if (name === "core/gallery") {
    //console.log(settings, name);
    const supportsLightbox = blocksWithLightboxSupport.includes(name);
    // Add custom attribute
    if (supportsLightbox) {
      if (!settings?.attributes?.lightbox) {
        settings.attributes = Object.assign(settings.attributes, {
          lightbox: {
            type: 'boolean',
            default: false,
          },
          fancyLightbox: {
            type: 'boolean',
            default: false,
          },
        });
      }
    }
  }
  return settings;
}

/**
 * Add custom vc-blocks editor lightbox classes to selected blocks
 *
 * @param {Function} BlockEdit Original component.
 * @return {string} Wrapped component.
 */
const withLightboxClasses = createHigherOrderComponent((BlockListBlock) => {
  return (props) => {
    const {name, attributes} = props;
    const {lightbox, fancyLightbox} = attributes;
    const supportsLightbox = blocksWithLightboxSupport.includes(name);

    let wrapperProps;
    let newClassName = props.className;

    if (supportsLightbox) {
      wrapperProps = props?.wrapperProps;
      newClassName = classnames(props.className,
        {
          [`has-lightbox`]: lightbox,
          [`has-fancy-lightbox`]: fancyLightbox
        }
      );
    }
    wrapperProps = {...wrapperProps, 'data-animation': 'fadeIn'}

    return <BlockListBlock {...props} wrapperProps={wrapperProps} className={newClassName}/>;
  };
}, 'withlightboxClasses');

/**
 * Override props assigned to save component to inject lightbox classes.
 *
 * @param {Object} extraProps Additional props applied to save element.
 * @param {Object} blockType  Block type.
 * @param {Object} attributes Current block attributes.
 *
 * @return {Object} Filtered props applied to save element.
 */
function applyLightboxClasses(extraProps, blockType, attributes) {
  const supportsLightbox = blocksWithLightboxSupport.includes(blockType?.name);

  if (supportsLightbox) {
    const {lightbox, fancyLightbox} = attributes;
    extraProps = {...extraProps, 'data-animation': 'fadeIn'}
    extraProps.className = classnames(extraProps.className,
      {
        [`has-lightbox`]: lightbox,
        [`has-fancy-lightbox`]: fancyLightbox
      }
    );
  }

  return extraProps;
}

addFilter("blocks.registerBlockType", "vc-blocks/lightbox/add-attributes", addAttributes);
addFilter("editor.BlockEdit", "vc-blocks/lightbox/with-lightbox", withLightbox);
addFilter('editor.BlockListBlock', 'vc-blocks/lightbox/editor-lightbox-classes', withLightboxClasses);
addFilter('blocks.getSaveContent.extraProps', 'vc-blocks/lightbox/save-lightbox-classes', applyLightboxClasses);
