import assign from "lodash/assign";
import { createHigherOrderComponent } from "@wordpress/compose";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, RangeControl } from "@wordpress/components";
import { addFilter } from "@wordpress/hooks";
import { __ } from "@wordpress/i18n";

// Enable style controls on the following blocks
const disableStyleControlOnBlocks = [
  "vc-blocks/breadcrumb",
  "vc-blocks/swiper-testimonials",
  "vc-blocks/post-filter",
  "vc-blocks/latest-posts",
];

const spacingClasses = {
  pt: ["pt-1", "pt-2", "pt-3", "pt-4", "pt-5", "pt-6", "pt-7", "pt-8"],
  pb: ["pb-1", "pb-2", "pb-3", "pb-4", "pb-5", "pb-6", "pb-7", "pb-8"],
  mt: ["mt-1", "mt-2", "mt-3", "mt-4", "mt-5", "mt-6", "mt-7", "mt-8"],
  mb: ["mb-1", "mb-2", "mb-3", "mb-4", "mb-5", "mb-6", "mb-7", "mb-8"],
};

// Available spacing control options
const spacingControlOptions = [
  {
    label: __("None", "vc-blocks"),
    value: 0
  },
  {
    label: __("Extra Small", "vc-blocks"),
    value: 1
  },
  {
    label: __("Small", "vc-blocks"),
    value: 2
  },
  {
    label: __("Medium", "vc-blocks"),
    value: 3
  },
  {
    label: __("Large", "vc-blocks"),
    value: 4
  },
  {
    label: __("Extra Large", "vc-blocks"),
    value: 5
  },
  {
    label: __("Extra Large 1", "vc-blocks"),
    value: 6
  },
  {
    label: __("Extra Large 2", "vc-blocks"),
    value: 7
  },
  {
    label: __("Extra Large 3", "vc-blocks"),
    value: 8
  }
];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addSpacingControlAttribute = (settings, name) => {
  //console.log(settings, name);
  // Use Lodash's assign to gracefully handle if attributes are undefined

  if (disableStyleControlOnBlocks.includes(name)) {
    return settings;
  }

  settings.attributes = assign(settings.attributes, {
    paddingTop: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingBottom: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingLeft: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingRight: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginTop: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginBottom: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginLeft: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginRight: {
      type: "number",
      default: spacingControlOptions[0].value
    }
  });

  return settings;
};

addFilter(
  "blocks.registerBlockType",
  "vc-blocks/attribute/spacing-controls",
  addSpacingControlAttribute
);

/**
 * Create HOC to add spacing control to inspector controls of block.
 */
const withSpacingControl = createHigherOrderComponent(BlockEdit => {
  return props => {
    //console.log('PROPS: ', props);
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return <BlockEdit {...props} />;
    }

    const {
      paddingTop,
      paddingBottom,
      //paddingLeft,
      //paddingRight,
      marginTop,
      marginBottom
      //marginLeft,
      //marginRight
    } = props.attributes;

    const onSpacingChange = (selectedSpacingOption, spacingType) => {
      if (!spacingType) {
        return true;
      }
      const { className } = props.attributes;
      //console.log(selectedSpacingOption, spacingType, spacingClasses[spacingType]);
      // Filter out spacing css classes to preserve other additional classes
      const classNameWithoutSpacing = className
        ? className
            .split(" ")
            .filter(
              classString => !spacingClasses[spacingType].includes(classString)
            )
        : [];

      let currentSpacingClasses = [];
      if (selectedSpacingOption > 0) {
        currentSpacingClasses.push(`${spacingType}-${selectedSpacingOption}`);
      }

      const allSpacingClasses = classNameWithoutSpacing.concat(
        currentSpacingClasses
      );
      //console.log(allSpacingClasses);
      const allSpacingClassesStr = allSpacingClasses.join(" ");

      let spacingTypeFullName = "";
      switch (spacingType) {
        case "pt":
          spacingTypeFullName = "paddingTop";
          break;
        case "pb":
          spacingTypeFullName = "paddingBottom";
          break;
        case "pl":
          spacingTypeFullName = "paddingLeft";
          break;
        case "pr":
          spacingTypeFullName = "paddingRight";
          break;
        case "mt":
          spacingTypeFullName = "marginTop";
          break;
        case "mb":
          spacingTypeFullName = "marginBottom";
          break;
        case "ml":
          spacingTypeFullName = "marginLeft";
          break;
        case "mr":
          spacingTypeFullName = "marginRight";
          break;
      }

      props.setAttributes({
        [spacingTypeFullName]: selectedSpacingOption
      });

      props.setAttributes({
        className: allSpacingClassesStr
      });
    };

    return (
      <>
        <BlockEdit {...props} />
        <InspectorControls>
          <PanelBody
            title={__("Spacing Controls", "vc-blocks")}
            initialOpen={false}
          >
            <RangeControl
              label={__("Margin Top", "vc-blocks")}
              value={marginTop}
              onChange={selectedSpacingOption =>
                onSpacingChange(selectedSpacingOption, "mt")
              }
              min={0}
              max={8}
            />
            <RangeControl
              label={__("Margin Bottom", "vc-blocks")}
              value={marginBottom}
              onChange={selectedSpacingOption =>
                onSpacingChange(selectedSpacingOption, "mb")
              }
              min={0}
              max={8}
            />

            <RangeControl
              label={__("Padding Top", "vc-blocks")}
              value={paddingTop}
              onChange={selectedSpacingOption =>
                onSpacingChange(selectedSpacingOption, "pt")
              }
              min={0}
              max={8}
            />
            <RangeControl
              label={__("Padding Bottom", "vc-blocks")}
              value={paddingBottom}
              onChange={selectedSpacingOption =>
                onSpacingChange(selectedSpacingOption, "pb")
              }
              min={0}
              max={8}
            />
          </PanelBody>
        </InspectorControls>
      </>
    );
  };
}, "withSpacingControl");

addFilter(
  "editor.BlockEdit",
  "vc-blocks/with-spacing-control",
  withSpacingControl
);

/**
 * Add margin style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
const addSpacingExtraProps = (saveElementProps, blockType, attributes) => {
  //console.log(saveElementProps);

  if (attributes.paddingTop) {
    // Use Lodash's assign to gracefully handle if attributes are undefined
    /* assign(saveElementProps, {
    /* assign(saveElementProps, {
       paddingTop:attributes.paddingTop
     });*/
  }
  //console.log(blockType, attributes);
  return saveElementProps;
};

addFilter(
  "blocks.getSaveContent.extraProps",
  "vc-blocks/get-save-content/extra-props",
  addSpacingExtraProps
);
