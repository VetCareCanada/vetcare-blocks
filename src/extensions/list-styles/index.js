import { __ } from "@wordpress/i18n";
import { registerBlockStyle } from "@wordpress/blocks";

// Default list style for reset.
registerBlockStyle("core/list", {
  name: "default",
  label: __("Default", "vc-blocks"),
  isDefault: true
});

registerBlockStyle("core/list", {
  name: "checkbox-icon",
  label: __("Checkbox Icon", "vc-blocks"),
  isDefault: false
});

registerBlockStyle("core/list", {
  name: "none",
  label: __("None", "vc-blocks"),
  isDefault: false
});
