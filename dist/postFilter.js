/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/blocks/post-filter/frontend/post-filter.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/blocks/post-filter/frontend/post-filter.js":
/*!********************************************************!*\
  !*** ./src/blocks/post-filter/frontend/post-filter.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var default_hash = "all";

window.onload = function (event) {
  var grid = document.querySelector(".post-filter .grid");
  var selectedFilter = location.hash.substring(1) ? document.querySelector('.post-filter .filter-tabs > .filter[data-slug="' + location.hash.substring(1) + '"]') : document.querySelector('.post-filter .filter-tabs > .filter[data-slug="all"]');
  console.log(selectedFilter);

  function displayPosts() {
    if (location.hash == "" || !selectedFilter || selectedFilter === null) {
      location.hash = default_hash;
    }

    if (selectedFilter) {
      selectedFilter.classList.add("active");
    }

    var _iterator = _createForOfIteratorHelper(selectedFilter.parentNode.children),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var sibling = _step.value;
        if (sibling !== selectedFilter) sibling.classList.remove("active");
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    grid.querySelectorAll(":scope > .grid-item").forEach(function (item) {
      return item.classList.remove("show");
    });
    grid.querySelectorAll("." + location.hash.substring(1)).forEach(function (item) {
      return item.classList.add("show");
    });
  }

  displayPosts();

  window.onhashchange = function () {
    displayPosts();
  };
};

document.addEventListener("DOMContentLoaded", function () {
  var filters = document.querySelectorAll(".post-filter .filter-tabs .filter");
  var mobileFilters = document.querySelectorAll(".post-filter .filter-tabs-mobile .filter");
  var postFilterMobileToggle = document.querySelector(".post-filter .post-filter-mobile-toggle");
  var mobileFilterTabsContent = document.querySelector(".post-filter .filter-tabs-mobile .filter-tabs-content");
  var currentFilter = document.querySelector(".post-filter .post-filter-mobile-toggle .current-filter");
  console.log(postFilterMobileToggle);
  postFilterMobileToggle.addEventListener("click", function (e) {
    this.classList.toggle("active");
    mobileFilterTabsContent.classList.toggle("hide");
  });

  function currentFilterInit() {
    var _iterator2 = _createForOfIteratorHelper(mobileFilters),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var mobileFilter = _step2.value;

        if (mobileFilter.getAttribute("data-slug") === location.hash.substring(1)) {
          return mobileFilter.textContent;
        } else {
          return default_hash;
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }

  currentFilter.innerHTML = currentFilterInit(); // Filter

  var _iterator3 = _createForOfIteratorHelper(filters),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      var filter = _step3.value;
      filter.addEventListener("click", function () {
        location.hash = this.getAttribute("data-slug");
      });
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }

  var _iterator4 = _createForOfIteratorHelper(mobileFilters),
      _step4;

  try {
    var _loop = function _loop() {
      var mobileFilter = _step4.value;
      mobileFilter.addEventListener("click", function () {
        location.hash = this.getAttribute("data-slug");
        currentFilter.innerHTML = mobileFilter.textContent ? mobileFilter.textContent : default_hash;
      });
    };

    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      _loop();
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }
});

/***/ })

/******/ });
//# sourceMappingURL=postFilter.js.map