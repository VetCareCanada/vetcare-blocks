/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/hoc/lightbox/frontend/vc-lightbox.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/hoc/lightbox/frontend/vc-lightbox.js":
/*!**************************************************!*\
  !*** ./src/hoc/lightbox/frontend/vc-lightbox.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _vc_lightbox_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vc-lightbox.scss */ "./src/hoc/lightbox/frontend/vc-lightbox.scss");
/* harmony import */ var _vc_lightbox_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_vc_lightbox_scss__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




(function ($) {
  'use strict'; //swiper init config used for gallery lightbox.

  var swiperConfig = {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets'
    },
    keyboard: {
      enabled: true
    }
  }; //get all the wp-gallery blocks that has the lightbox enabled.

  var lightboxGalleries = document.getElementsByClassName('has-lightbox'); //loop through all lightbox galleries, add indexed class and call the renderLightboxModal function for each of them.

  Array.from(lightboxGalleries).forEach(function (lightbox, index) {
    lightbox.className += ' lightbox-' + index + ' ';
    var hasFancyLightbox = lightbox.classList.contains("has-fancy-lightbox");
    renderLightboxModal(index, hasFancyLightbox);
  }); //create and append modals to body tag for each gallery added to the given page

  function renderLightboxModal(lightboxIndex, hasFancyLightbox) {
    var images = document.querySelectorAll(".has-lightbox.lightbox-".concat(lightboxIndex, " figure img"));

    if (images.length > 0) {
      //append lightbox modal to body
      $("body").append(getLightboxModalHtml(images, lightboxIndex, hasFancyLightbox));
      var lightboxSwiperInit; //add click event to images.

      Array.from(images).forEach(function (image, imageIndex) {
        image.closest('figure').addEventListener("click", function () {
          $('#vcLightboxModal-' + lightboxIndex).modal("show"); //initialize the lightbox swiper slider

          lightboxSwiperInit = new Swiper("#lightboxSwiper-" + lightboxIndex, _objectSpread(_objectSpread({}, swiperConfig), {}, {
            init: false,
            initialSlide: imageIndex
          }));

          if (hasFancyLightbox) {
            lightboxSwiperInit.on('init slideChange', function () {
              if (images[this.activeIndex]) {
                var activeImageSrc = images[this.activeIndex].getAttribute('src');
                document.getElementById("vcLightboxModal-".concat(lightboxIndex)).style.setProperty("--fancy-background-image", "url(\"".concat(activeImageSrc, "\")"));
              }
            });
          }

          lightboxSwiperInit.init();
        });
      }); //Destroy the swiper instance when modal is closed.

      $('#vcLightboxModal-' + lightboxIndex).on('hidden.bs.modal', function () {
        lightboxSwiperInit.destroy();
      });
    }
  } //return the lightbox modal html with swiper slider markup


  function getLightboxModalHtml(images, lightboxIndex, hasFancyLightbox) {
    var swiperSlides = "";
    Array.from(images).forEach(function (image) {
      swiperSlides += "<div class=\"swiper-slide text-center\">".concat(image.outerHTML, "</div>");
    });
    return "\n        <div id=\"vcLightboxModal-".concat(lightboxIndex, "\" class=\"modal vc-lightbox-modal\" tabindex=\"-1\">\n            <div class=\"modal-dialog modal-dialog-centered\">\n\n                <div class=\"modal-content\">\n                    <div class=\"modal-body\">\n                        <a href=\"javascript:;\" class=\"close\" data-bs-dismiss=\"modal\" aria-label=\"Close\">\n                            <span aria-hidden=\"true\">&#x2715;</span>\n                        </a>\n                        <div class=\"swiper-container\" id=\"lightboxSwiper-").concat(lightboxIndex, "\">\n                            <div class=\"swiper-wrapper\">\n                                ").concat(swiperSlides, "\n                            </div>\n                            <div class=\"swiper-pagination\"></div>\n                            <div class=\"swiper-button-prev\"></div>\n                            <div class=\"swiper-button-next\"></div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>");
  }
})(jquery__WEBPACK_IMPORTED_MODULE_0___default.a);

/***/ }),

/***/ "./src/hoc/lightbox/frontend/vc-lightbox.scss":
/*!****************************************************!*\
  !*** ./src/hoc/lightbox/frontend/vc-lightbox.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ })

/******/ });
//# sourceMappingURL=vcLightbox.js.map