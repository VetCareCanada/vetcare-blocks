/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/blocks/accordion/script.js":
/*!****************************************!*\
  !*** ./src/blocks/accordion/script.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/accordion/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/accordion/style.scss":
/*!*****************************************!*\
  !*** ./src/blocks/accordion/style.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/cards/script.js":
/*!************************************!*\
  !*** ./src/blocks/cards/script.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/cards/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/cards/style.scss":
/*!*************************************!*\
  !*** ./src/blocks/cards/style.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/cta/script.js":
/*!**********************************!*\
  !*** ./src/blocks/cta/script.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/cta/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/cta/style.scss":
/*!***********************************!*\
  !*** ./src/blocks/cta/style.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/fancy-heading/script.js":
/*!********************************************!*\
  !*** ./src/blocks/fancy-heading/script.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/fancy-heading/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/fancy-heading/style.scss":
/*!*********************************************!*\
  !*** ./src/blocks/fancy-heading/style.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/img-text/script.js":
/*!***************************************!*\
  !*** ./src/blocks/img-text/script.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/img-text/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/img-text/style.scss":
/*!****************************************!*\
  !*** ./src/blocks/img-text/style.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/latest-posts/script.js":
/*!*******************************************!*\
  !*** ./src/blocks/latest-posts/script.js ***!
  \*******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles.scss */ "./src/blocks/latest-posts/styles.scss");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/latest-posts/styles.scss":
/*!*********************************************!*\
  !*** ./src/blocks/latest-posts/styles.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/mailchimp/script.js":
/*!****************************************!*\
  !*** ./src/blocks/mailchimp/script.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/mailchimp/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/mailchimp/style.scss":
/*!*****************************************!*\
  !*** ./src/blocks/mailchimp/style.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/map-text/script.js":
/*!***************************************!*\
  !*** ./src/blocks/map-text/script.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/map-text/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/map-text/style.scss":
/*!****************************************!*\
  !*** ./src/blocks/map-text/style.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/multi-cta/script.js":
/*!****************************************!*\
  !*** ./src/blocks/multi-cta/script.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/multi-cta/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/multi-cta/style.scss":
/*!*****************************************!*\
  !*** ./src/blocks/multi-cta/style.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/post-filter/script.js":
/*!******************************************!*\
  !*** ./src/blocks/post-filter/script.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/post-filter/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);
 // var default_hash = "all";
// window.onload = function(event) {
//   var grid = document.querySelector(".post-filter .grid");
//   var selectedFilter = location.hash.substring(1) ? document.querySelector(
//     '.post-filter .filter-tabs > .filter[data-slug="' +
//       location.hash.substring(1) +
//       '"]'
//   ) :  document.querySelector(
//     '.post-filter .filter-tabs > .filter[data-slug="all"]'
//   );
//   console.log(selectedFilter);
//   function displayPosts() {
//     if (location.hash == "" || !selectedFilter || selectedFilter === null) {
//       location.hash = default_hash;
//     }
//     if (selectedFilter) {
//       selectedFilter.classList.add("active");
//     }
//     for (let sibling of selectedFilter.parentNode.children) {
//       if (sibling !== selectedFilter) sibling.classList.remove("active");
//     }
//     grid
//       .querySelectorAll(":scope > div")
//       .forEach(item => item.classList.remove("show"));
//     grid
//       .querySelectorAll("." + location.hash.substring(1))
//       .forEach(item => item.classList.add("show"));
//   }
//   displayPosts();
//   window.onhashchange = function() {
//     displayPosts();
//   };
// };
// document.addEventListener("DOMContentLoaded", function() {
//   var filters = document.querySelectorAll(".post-filter .filter-tabs .filter");
//   var mobileFilters = document.querySelectorAll(
//     ".post-filter .filter-tabs-mobile .filter"
//   );
//   var postFilterMobileToggle = document.querySelector(
//     ".post-filter .post-filter-mobile-toggle"
//   );
//   var mobileFilterTabsContent = document.querySelector(
//     ".post-filter .filter-tabs-mobile .filter-tabs-content"
//   );
//   var currentFilter = document.querySelector(
//     ".post-filter .post-filter-mobile-toggle .current-filter"
//   );
//   postFilterMobileToggle.addEventListener("click", function(e) {
//     this.classList.toggle("active");
//     mobileFilterTabsContent.classList.toggle("hide");
//   });
//   function currentFilterInit() {
//     for (let mobileFilter of mobileFilters) {
//       if (
//         mobileFilter.getAttribute("data-slug") === location.hash.substring(1)
//       ) {
//         return mobileFilter.textContent;
//       }
//     }
//   }
//   currentFilter.innerHTML = currentFilterInit();
//   // Filter
//   for (let filter of filters) {
//     filter.addEventListener("click", function() {
//       location.hash = this.getAttribute("data-slug");
//     });
//   }
//   for (let mobileFilter of mobileFilters) {
//     mobileFilter.addEventListener("click", function() {
//       location.hash = this.getAttribute("data-slug");
//       currentFilter.innerHTML = mobileFilter.textContent
//         ? mobileFilter.textContent
//         : default_hash;
//     });
//   }
// });

/***/ }),

/***/ "./src/blocks/post-filter/style.scss":
/*!*******************************************!*\
  !*** ./src/blocks/post-filter/style.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/pricing-table/script.js":
/*!********************************************!*\
  !*** ./src/blocks/pricing-table/script.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/pricing-table/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/pricing-table/style.scss":
/*!*********************************************!*\
  !*** ./src/blocks/pricing-table/style.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/swiper-slider/script.js":
/*!********************************************!*\
  !*** ./src/blocks/swiper-slider/script.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/swiper-slider/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/swiper-slider/style.scss":
/*!*********************************************!*\
  !*** ./src/blocks/swiper-slider/style.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/team-member/script.js":
/*!******************************************!*\
  !*** ./src/blocks/team-member/script.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/team-member/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/team-member/style.scss":
/*!*******************************************!*\
  !*** ./src/blocks/team-member/style.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/vctab-pane/script.js":
/*!*****************************************!*\
  !*** ./src/blocks/vctab-pane/script.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/vctab-pane/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/vctab-pane/style.scss":
/*!******************************************!*\
  !*** ./src/blocks/vctab-pane/style.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/vctab/script.js":
/*!************************************!*\
  !*** ./src/blocks/vctab/script.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/vctab/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/blocks/vctab/style.scss":
/*!*************************************!*\
  !*** ./src/blocks/vctab/style.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/blocks/video-modal/script.js":
/*!******************************************!*\
  !*** ./src/blocks/video-modal/script.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/blocks/video-modal/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);



(function ($) {
  $(document).ready(function () {
    var $videoSrc;
    $(".video-link a, .video-link .swiper-slide__img i").on("click", function (e) {
      $videoSrc = $(this).attr("href") || $(this).prev('img').attr("data-videourl");
      var videoUrlObj = parseVideo($videoSrc);

      if (videoUrlObj.type !== "youtube" && videoUrlObj.type !== "vimeo") {
        return true;
      }

      $videoSrc = createEmbedUrl($videoSrc); //console.log($videoSrc);

      e.preventDefault(); //check is modal is already added to the site

      if ($("#video-modal").length <= 0) {
        var modalHtml = "\n          <div id=\"video-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n            <div class=\"modal-dialog modal-xl modal-dialog-centered\" role=\"document\">\n                <div class=\"modal-content\">\n                    <div class=\"modal-body\">\n                      <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>\n                        <!-- 16:9 aspect ratio -->\n                        <div class=\"ratio ratio-16x9\">\n                            <iframe class=\"embed-responsive-item\" src=\"\" id=\"video-modal-iframe\" allowscriptaccess=\"always\" allow=\"autoplay\"></iframe>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n      ";
        $("body").append(modalHtml); // when the modal is opened autoplay it

        $("#video-modal").on("show.bs.modal", function () {
          $("#video-modal-iframe").attr("src", $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        }); // stop playing the youtube video when I close the modal

        $("#video-modal").on("hide.bs.modal", function () {
          $("#video-modal-iframe").attr("src", "");
        });
      } //show the modal


      $("#video-modal").modal("show");
    });

    function parseVideo(url) {
      // - Supported YouTube URL formats:
      //   - http://www.youtube.com/watch?v=My2FRPA3Gf8
      //   - http://youtu.be/My2FRPA3Gf8
      //   - https://youtube.googleapis.com/v/My2FRPA3Gf8
      // - Supported Vimeo URL formats:
      //   - http://vimeo.com/25451551
      //   - http://player.vimeo.com/video/25451551
      // - Also supports relative URLs:
      //   - //player.vimeo.com/video/25451551
      url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);
      var type = "";

      if (RegExp.$3.indexOf("youtu") > -1) {
        type = "youtube";
      } else if (RegExp.$3.indexOf("vimeo") > -1) {
        type = "vimeo";
      }

      return {
        type: type,
        id: RegExp.$6
      };
    }

    function createEmbedUrl(url) {
      var videoObj = parseVideo(url);
      var embedUrl = "";

      if (videoObj.type === "youtube") {
        embedUrl = "//www.youtube.com/embed/" + videoObj.id;
      } else if (videoObj.type === "vimeo") {
        embedUrl = "//player.vimeo.com/video/" + videoObj.id;
      }

      return embedUrl;
    }
  });
})(jquery__WEBPACK_IMPORTED_MODULE_1___default.a);

/***/ }),

/***/ "./src/blocks/video-modal/style.scss":
/*!*******************************************!*\
  !*** ./src/blocks/video-modal/style.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/extensions/list-styles/checkbox-rounded.svg":
/*!*********************************************************!*\
  !*** ./src/extensions/list-styles/checkbox-rounded.svg ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "./images/checkbox-rounded.svg";

/***/ }),

/***/ "./src/extensions/list-styles/script.js":
/*!**********************************************!*\
  !*** ./src/extensions/list-styles/script.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/extensions/list-styles/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);
__webpack_require__(/*! ./checkbox-rounded.svg */ "./src/extensions/list-styles/checkbox-rounded.svg");



/***/ }),

/***/ "./src/extensions/list-styles/style.scss":
/*!***********************************************!*\
  !*** ./src/extensions/list-styles/style.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/hoc/spacing-controls/script.js":
/*!********************************************!*\
  !*** ./src/hoc/spacing-controls/script.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/hoc/spacing-controls/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./src/hoc/spacing-controls/style.scss":
/*!*********************************************!*\
  !*** ./src/hoc/spacing-controls/style.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/script.js":
/*!***********************!*\
  !*** ./src/script.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.scss */ "./src/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _extensions_list_styles_script__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./extensions/list-styles/script */ "./src/extensions/list-styles/script.js");
/* harmony import */ var _hoc_spacing_controls_script__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hoc/spacing-controls/script */ "./src/hoc/spacing-controls/script.js");
/* harmony import */ var _blocks_team_member_script__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blocks/team-member/script */ "./src/blocks/team-member/script.js");
/* harmony import */ var _blocks_latest_posts_script__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./blocks/latest-posts/script */ "./src/blocks/latest-posts/script.js");
/* harmony import */ var _blocks_cta_script__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./blocks/cta/script */ "./src/blocks/cta/script.js");
/* harmony import */ var _blocks_img_text_script__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./blocks/img-text/script */ "./src/blocks/img-text/script.js");
/* harmony import */ var _blocks_accordion_script__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./blocks/accordion/script */ "./src/blocks/accordion/script.js");
/* harmony import */ var _blocks_map_text_script__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./blocks/map-text/script */ "./src/blocks/map-text/script.js");
/* harmony import */ var _blocks_video_modal_script__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./blocks/video-modal/script */ "./src/blocks/video-modal/script.js");
/* harmony import */ var _blocks_multi_cta_script__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./blocks/multi-cta/script */ "./src/blocks/multi-cta/script.js");
/* harmony import */ var _blocks_cards_script__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./blocks/cards/script */ "./src/blocks/cards/script.js");
/* harmony import */ var _blocks_swiper_slider_script__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./blocks/swiper-slider/script */ "./src/blocks/swiper-slider/script.js");
/* harmony import */ var _blocks_pricing_table_script__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./blocks/pricing-table/script */ "./src/blocks/pricing-table/script.js");
/* harmony import */ var _blocks_fancy_heading_script__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./blocks/fancy-heading/script */ "./src/blocks/fancy-heading/script.js");
/* harmony import */ var _blocks_post_filter_script__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./blocks/post-filter/script */ "./src/blocks/post-filter/script.js");
/* harmony import */ var _blocks_mailchimp_script__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./blocks/mailchimp/script */ "./src/blocks/mailchimp/script.js");
/* harmony import */ var _blocks_vctab_script__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./blocks/vctab/script */ "./src/blocks/vctab/script.js");
/* harmony import */ var _blocks_vctab_pane_script__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./blocks/vctab-pane/script */ "./src/blocks/vctab-pane/script.js");


 //import "./blocks/base/script";
//import "./blocks/secondblock/script";









 //import "./blocks/slider/script";

 // import "./blocks/swiper/script";








/***/ }),

/***/ "./src/style.scss":
/*!************************!*\
  !*** ./src/style.scss ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ })

/******/ });
//# sourceMappingURL=script.js.map