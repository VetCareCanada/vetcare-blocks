<?php

/**
 * Plugin name: vc-blocks
 * Author: Gurpreet
 */

if (!defined('ABSPATH')) {
    exit();
}

require_once 'src/blocks/latest-posts/latest-posts.php'; // Frontend Rendering for Latest Posts Block
//require_once 'src/blocks/breadcrumb/index.php'; // Frontend Rendering for Testimonials Slider Block
//require_once 'src/blocks/swiper-testimonials/index.php'; // Frontend Rendering for Testimonials Slider Block
//require_once 'src/blocks/post-filter/index.php'; // Frontend Rendering for Post Filter Block
// require_once 'src/blocks/product-info/product-info.php'; // Frontend Rendering for Product Info Block

function vc_blocks_categories($categories, $post)
{
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'vc-category',
                'title' => __('VC Blocks', 'vc-blocks'),
                'icon' => 'wordpress',
            ),
        )
    );
}

add_filter('block_categories', 'vc_blocks_categories', 10, 2);

add_action('enqueue_block_editor_assets', 'vc_blocks_enqueue_assets');

function vc_blocks_enqueue_assets()
{
    wp_enqueue_script(
        'vc-blocks-editor-js',
        plugins_url('dist/editor_script.js', __FILE__),
        array('wp-data')
    );
}

add_action('wp_enqueue_scripts', 'vc_blocks_enqueue_frontend_assets');
/**
 * Enqueue Swiperjs when needed
 */
function vc_blocks_enqueue_frontend_assets()
{
    $id = get_the_ID();
    $has_wp_gallery_block = has_block('gallery', $id);
    $has_swiper_testimonials_block = has_block('vc-blocks/swiper-testimonials', $id);
    $has_swiper_slider_block = has_block('vc-blocks/swiper-slider', $id);
    $has_swiper_block = has_block('vc-blocks/swiper', $id);
    $has_post_filter_block = has_block('vc-blocks/post-filter', $id);
    $has_banner_block = has_block('vc-blocks/banner', $id);
    $has_video_banner_block = has_block('vc-blocks/video-banner', $id);
    $add_swiper_globally = get_field('add_swiper_globally', 'option');

    if ($add_swiper_globally || $has_wp_gallery_block || $has_swiper_testimonials_block ||
        $has_swiper_slider_block || $has_swiper_block || $has_banner_block) {
        // enqueue swiper js.
        wp_enqueue_script(
            'swiper-js',
            plugins_url('dist/swiper.js', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/swiper.js'),
            true
        );

        // enqueue swiper css.
        wp_enqueue_style(
            'swiper-css',
            plugins_url('dist/swiper.css', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/swiper.css')
        );

        if ($has_swiper_slider_block || $add_swiper_globally) {
            // enqueue swiper testimonials js.
            wp_enqueue_script(
                'swiper-slider-js',
                plugins_url('dist/swiperSlider.js', __FILE__),
                array(),
                filemtime(plugin_dir_path(__FILE__) . 'dist/swiperSlider.js'),
                true
            );
        }

        if ($has_swiper_testimonials_block) {
            // enqueue swiper testimonials js.
            wp_enqueue_script(
                'swiper-testimonials-js',
                plugins_url('dist/swiperTestimonials.js', __FILE__),
                array(),
                filemtime(plugin_dir_path(__FILE__) . 'dist/swiperTestimonials.js'),
                true
            );
        }
    }

    if ($has_wp_gallery_block) {
        wp_enqueue_script(
            'vc-lightbox-js',
            plugins_url('dist/vcLightbox.js', __FILE__),
            array('jquery'),
            filemtime(plugin_dir_path(__FILE__) . 'dist/vcLightbox.js'),
            true
        );

        wp_enqueue_style(
            'vcLightbox-css',
            plugins_url('dist/vcLightbox.css', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/vcLightbox.css')
        );
    }

    if ($has_post_filter_block) {
        wp_enqueue_script(
            'post-filter-js',
            plugins_url('dist/postFilter.js', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/postFilter.js'),
            true
        );
    }

    // banners
    if ($has_banner_block) {
        wp_enqueue_style(
            'banner-css',
            plugins_url('dist/banner.css', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/banner.css')
        );
    }

    // video banner.
    if ($has_video_banner_block) {
        wp_enqueue_script(
            'jquery-youtube-background-js',
            plugins_url('dist/jquery.youtube-background.js', __FILE__),
            array('jquery'),
            filemtime(plugin_dir_path(__FILE__) . 'dist/jquery.youtube-background.js'),
            true
        );
        wp_enqueue_script(
            'vc-video-banner-js',
            plugins_url('dist/videoBanner.js', __FILE__),
            array('jquery', 'jquery-youtube-background-js'),
            filemtime(plugin_dir_path(__FILE__) . 'dist/videoBanner.js'),
            true
        );

        wp_enqueue_style(
            'video-banner-css',
            plugins_url('dist/videoBanner.css', __FILE__),
            array(),
            filemtime(plugin_dir_path(__FILE__) . 'dist/videoBanner.css')
        );
    }
}

/**
 * Register the blocks
 *
 * @param string $block block name.
 * @param array $options block registration options.
 */
function vc_register_block_type($block, $options = array())
{
    $merged_options = array_merge(
        array(
            'editor_script' => 'vc-blocks-editor-script',
            'editor_style' => array('vc-blocks-editor-style'),
            'script' => 'vc-blocks-script',
            'style' => 'vc-blocks-style',
        ),
        $options
    );
    //echo $block;
    //pr($merged_options);
    register_block_type(
        'vc-blocks/' . $block,
        $merged_options
    );
}

function vc_blocks_register()
{
    wp_register_script(
        'vc-blocks-editor-script',
        plugins_url('dist/editor.js', __FILE__),
        array(
            'wp-blocks',
            'wp-i18n',
            'wp-element',
            'wp-block-editor',
            'wp-components',
            'lodash',
            'wp-blob',
            'wp-data',
            'wp-html-entities',
            'wp-compose',
            'wp-hooks',
            'wp-server-side-render',
        )
    );

    wp_register_script(
        'vc-blocks-script',
        plugins_url('dist/script.js', __FILE__),
        array('jquery')
    );

    wp_register_style(
        'vc-blocks-editor-style',
        plugins_url('dist/editor.css', __FILE__),
        array('wp-edit-blocks'),
        filemtime(plugin_dir_path(__FILE__) . 'dist/editor.css')
    );

    wp_register_style(
        'vc-blocks-style',
        plugins_url('dist/style.css', __FILE__),
        // array( 'swiper-css' ),
        filemtime(plugin_dir_path(__FILE__) . 'dist/style.css')
    );

    vc_register_block_type('base');
    vc_register_block_type('vctabs');
    vc_register_block_type('vctab');
    vc_register_block_type('vctab-content');
    vc_register_block_type('vctab-pane');
    vc_register_block_type('team-member');
    vc_register_block_type('team-members');
    vc_register_block_type('cta');
    vc_register_block_type(
        'testimonials',
        array(
            'render_callback' => 'vc_blocks_render_testimonials_block', // Reference /testimonials/testimonials.php.
            'attributes' => array(
                'postType' => array(
                    'type' => 'string',
                    'default' => 'vc_testimonial',
                ),
                'layoutType' => array(
                    'type' => 'string',
                    'default' => 'slider',
                ),
                'sliderCount' => array(
                    'type' => 'number',
                    'default' => '3',
                ),
                'sliderDots' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderArrows' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderAutoplay' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderSpeed' => array(
                    'type' => 'number',
                    'default' => '300',
                ),
                'sliderFade' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderInfinite' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderLazyload' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderSlidesToShow' => array(
                    'type' => 'number',
                    'default' => '1',
                ),
                'sliderSlidesToScroll' => array(
                    'type' => 'number',
                    'default' => '1',
                ),
                'sliderCenterMode' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderCenterPadding' => array(
                    'type' => 'string',
                    'default' => '200px',
                ),
                'showTitle' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'excerpt' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderResponsive' => array(
                    'type' => 'array',
                    'default' => array(
                        array(
                            'breakpoint' => 992,
                            'settings' => array(
                                'slidesToShow' => 1,
                                'slidesToScroll' => 1,
                                'centerMode' => false,
                                'centerPadding' => '0px',
                            ),
                        ),
                    ),
                ),
            ),
        )
    );

    vc_register_block_type(
        'latest-posts',
        array(
            'render_callback' => 'vc_blocks_render_latest_posts_block',
            'attributes' => array(
                'numberOfPosts' => array(
                    'type' => 'number',
                    'default' => 2,
                ),
                'postType' => array(
                    'type' => 'string',
                    'default' => 'post',
                )
            ),
        )
    );
    vc_register_block_type('contact-us');
    vc_register_block_type('img-text');
    vc_register_block_type('accordion');
    vc_register_block_type('accordion-item');
    vc_register_block_type('banner');
    vc_register_block_type('map-text');
    vc_register_block_type('multi-cta');
    vc_register_block_type('slider');
    vc_register_block_type('mailchimp');
    vc_register_block_type('swiper-slider');
    vc_register_block_type('swiper-single-slide');
    vc_register_block_type('video-banner');
    vc_register_block_type('cards');
    vc_register_block_type('card');
    vc_register_block_type(
        'post-filter',
        array(
            'render_callback' => 'vc_blocks_render_post_filter',
            'attributes' => array(
                'postType' => array(
                    'type' => 'string',
                    'default' => 'post',
                ),
                'taxonomy' => array(
                    'type' => 'string',
                    'default' => 'category',
                ),
                'tabAlignment' => array(
                    'type' => 'string',
                    'default' => 'tab-left',
                ),
            ),
        )
    );
    vc_register_block_type(
        'breadcrumb',
        array(
            'render_callback' => 'vc_blocks_render_breadcrumb_block',
            'attributes' => array(
                'alignment' => array(
                    'type' => 'string',
                    'default' => 'text-center',
                ),
            ),
        )
    );

    vc_register_block_type(
        'swiper-testimonials',
        array(
            'render_callback' => 'vc_blocks_render_swiper_testimonials',
            'attributes' => array(
                'className' => array(
                    'type' => 'string',
                ),
                'layoutType' => array(
                    'type' => 'string',
                    'default' => 'slider',
                ),
                'postType' => array(
                    'type' => 'string',
                    'default' => 'vc_testimonial',
                ),
                'showTitle' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderFetchCount' => array(
                    'type' => 'number',
                    'default' => 5,
                ),
                'sliderSlidesPerView' => array(
                    'type' => 'number',
                    'default' => 1,
                ),
                'sliderSlidesToScroll' => array(
                    'type' => 'number',
                    'default' => 1,
                ),
                'sliderDots' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderArrows' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderFade' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderAutoplay' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'sliderInfinite' => array(
                    'type' => 'boolean',
                    'default' => true,
                ),
                'excerpt' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
                'sliderAutoplayDelay' => array(
                    'type' => 'number',
                    'default' => '3000',
                ),
                'showImage' => array(
                    'type' => 'boolean',
                    'default' => false,
                ),
            ),
        )
    );
}

add_action('init', 'vc_blocks_register');
