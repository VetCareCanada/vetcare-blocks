const autoprefixer = require("autoprefixer");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const CleanPlugin = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = (env, argv) => {
  function isDevelopment() {
    return argv.mode === "development";
  }

  var config = {
    entry: {
      editor: "./src/editor.js",
      script: "./src/script.js",
      editor_script: "./src/editor_script.js",
      swiper: "./src/swiper.js", // this is for Swiper js and Swiper css.
      vcLightbox: "./src/hoc/lightbox/frontend/vc-lightbox.js",
      swiperTestimonials: "./src/blocks/swiper-testimonials/frontend/swiper-testimonials.js",
      swiperSlider: "./src/blocks/swiper-slider/frontend/swiper-slider.js",
      postFilter: "./src/blocks/post-filter/frontend/post-filter.js",
      banner: "./src/blocks/banner/frontend/banner.js",
      videoBanner: "./src/blocks/video-banner/frontend/video-banner.js"
    },
    output: {
      filename: "[name].js"
    },
    optimization: {
      minimizer: [
        new TerserPlugin({
          sourceMap: true
        }),
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            map: {
              inline: false,
              annotation: true
            }
          }
        })
      ]
    },
    plugins: [
      new CleanPlugin(),
      new MiniCSSExtractPlugin({
        chunkFilename: "[id].css",
        filename: chunkData => {
          return chunkData.chunk.name === "script" ? "style.css" : "[name].css";
        }
      }),
      new CopyPlugin({
        patterns: [
          {from: "./node_modules/youtube-background/jquery.youtube-background.js", to: "jquery.youtube-background.js"}
        ],
      }),
    ],
    devtool: isDevelopment() ? "cheap-module-source-map" : "source-map",
    module: {
      rules: [
        {
          test: /\.js$/,
          /* exclude: [
            /node_modules/,
            /testimonials/
          ],*/

          exclude: [
            /node_modules/,
            /src\/blocks\/testimonials\/js/,
            /src\/blocks\/testimonials\/dist/
          ],
          use: [
            {
              loader: "babel-loader",
              options: {
                plugins: ["@babel/plugin-proposal-class-properties"],
                presets: [
                  "@babel/preset-env",
                  [
                    "@babel/preset-react",
                    {
                      pragma: "wp.element.createElement",
                      pragmaFrag: "wp.element.Fragment",
                      development: isDevelopment()
                    }
                  ]
                ]
              }
            },
            "eslint-loader"
          ]
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCSSExtractPlugin.loader,
            "css-loader",
            {
              loader: "postcss-loader",
              options: {
                plugins: [autoprefixer()]
              }
            },
            "sass-loader"
          ]
        },
        {
          test: /\.(eot|woff|woff2|ttf)$/,
          use: {
            loader: "url-loader",
            options: {
              esModule: false,
              limit: false,
              name: "./fonts/[name].[ext]"
            }
          }
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: {
            loader: "url-loader",
            options: {
              esModule: false,
              limit: 10000,
              name: "./images/[name].[ext]"
            }
          }
        },
        {
          test: /\.(svg)$/,
          use: {
            loader: "url-loader",
            options: {
              esModule: false,
              limit: false,
              name: "./images/[name].[ext]"
            }
          }
        }
      ]
    },
    externals: {
      jquery: "jQuery",
      Swiper: "Swiper",
      "@wordpress/blocks": ["wp", "blocks"],
      "@wordpress/i18n": ["wp", "i18n"],
      "@wordpress/components": ["wp", "components"],
      "@wordpress/element": ["wp", "element"],
      "@wordpress/blob": ["wp", "blob"],
      "@wordpress/data": ["wp", "data"],
      "@wordpress/html-entities": ["wp", "htmlEntities"],
      "@wordpress/compose": ["wp", "compose"],
      "@wordpress/hooks": ["wp", "hooks"],
      "@wordpress/block-editor": ["wp", "blockEditor"],
      "@wordpress/server-side-render": ["wp", "serverSideRender"]
    }
  };
  return config;
};
